<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
	<head>
		<title>Save analyse</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<link rel="stylesheet" media="screen" href="stylesheets/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="stylesheets/default.css">
        <link rel="stylesheet" media="screen" href="stylesheets/list.css">
        <link rel="stylesheet" media="screen" href="stylesheets/status.css">
        <link rel="stylesheet" media="screen" href="stylesheets/feedlist.css">
        <link rel="stylesheet" media="screen" href="stylesheets/metro.css">
        <link rel="stylesheet" media="screen" href="stylesheets/login.css">
        <link rel="stylesheet" media="screen" href="stylesheets/wrap.css">
		
		<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="${_response_encoding}"></script>
		
		<meta charset="utf-8">

		<style>
                <%@include file='stylesheets/bootstrap.min.css' %>
                <%@include file='stylesheets/default.css' %>
                <%@include file='stylesheets/list.css' %>
                <%@include file='stylesheets/status.css' %>
                <%@include file='stylesheets/feedlist.css' %>
                <%@include file='stylesheets/metro.css' %>
                <%@include file='stylesheets/login.css' %>
                <%@include file='stylesheets/wrap.css' %>
            </style>
		
		<style type="text/css">
			body {
				padding-top: 40px;
				padding-bottom: 40px;
				background: url("${pageContext.request.contextPath}/images/background.jpg");
			  }
			  
			  .container {
				background-color:  rgba(105,105,105,0.6	);
				width: 100%;
			  }

			  .form-signup {
				padding: 19px 29px 29px;
			  }

			  
			  .form-signup .form-signup-heading
			  {
				margin-bottom: 10px;
				text-align: center;
			  }
			 
			  .type_container,
			  .time_container,
			  .date_container,
			  .doctor_container
 			  {
				font-size: 16px;
				margin-bottom: 5px;
				margin-right: 35%;
				padding: 2px 5px;
		      }
			  .form-signup button[type="submit"] {
				background-color: #708090;
			  }
		</style>
	</head>
	<body>
	<div class="container" >
    <button class="btn btn-large" type="submit"
            onclick="location.href='/Hospital/j_spring_security_logout.html'"
    			style="margin: 21px; float:right;">
    			Log out
    		</button>
      <form class="form-signup"  align="right">
	  <h3 class="form-signup-heading">Save analyse</h3>
	  <div class = "type_container">
		<p> Пациент
        <select type="roles" style="width: 300px; height: 35px">
			<option value="patient">Patient</option>
		</select></p> </div>
		
		<div class = "date_container">
		<p> Date
        <input type="date" style="width: 300px; height: 35px"> </p> </div>
		
		<div class = "time_container">
		<p> Result
        <input style="width: 300px; height: 35px"></p> </div>
		<button class="btn btn-large" type="submit" style="width: 200px; height: 35px; margin-right: 40.5%;">Ok</button>
      </form>

    </div> 
	</body>
</html>