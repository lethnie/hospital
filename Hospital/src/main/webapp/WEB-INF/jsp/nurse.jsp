<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Nurse</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/default.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/list.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/status.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/feedlist.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/metro.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/login.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/wrap.css">

    <script src="${pageContext.request.contextPath}/javascripts/bootstrap.min.js"></script>

    <!--<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>-->

    <script>
        function getHexRGBColor(color)
        {
            color = color.replace(/\s/g,"");
            var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);

            if(aRGB)
            {
                color = '';
                for (var i=1;  i<=3; i++) color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i])).toString(16).replace(/^(.)$/,'0$1');
            }
            else color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');

            return color;
        }

        $(document).ready(function(){
            $('#id_table_treatments tr').click(function() {
                if ($(this).attr('id') != "id_row_treatment") {
                    $(this).find('td').each (function() {
                        var col = $(this).css('backgroundColor');
                        var color = getHexRGBColor(col);
                        if (color != 'ffff99')
                            $(this).css('background', '#FFFF99');
                        else {
                            $(this).css('background', '#F8F8F8');
                        }
                    });
                }
            });

            $('#id_table_tests tr').click(function() {
                if ($(this).attr('id') != "id_row_test") {
                    $(this).find('td').each (function() {
                        var col = $(this).css('backgroundColor');
                        var color = getHexRGBColor(col);
                        if (color != 'ffff99')
                            $(this).css('background', '#FFFF99');
                        else {
                            $(this).css('background', '#F8F8F8');
                        }
                    });
                }
            });

            $('.count').click(function() {
                var count = $(this).text();
                var c = parseInt(count);
                c--;
                $(this).text(c);
            });
        });

            function sendDataToServer() {
            var jsonString = {};
            jsonString.tests = [];

            $('#id_table_tests tr').each(function() {
                if ($(this).attr('id') != "id_row_test") {
                    var col = $(this).find('td').css('backgroundColor');
                    var color = getHexRGBColor(col);
                    if (color == 'ffff99') {
                        var test = {};
                        test.id = $(this).attr('id');
                        test.type = $(this).find(".type").text();
                        test.patient = $(this).find(".patient").text();
                        test.result = $(this).find(".inresult").val();
                        jsonString.tests.push(test);
                    }
                }
            });
                //alert(JSON.stringify(jsonString));

            jsonString.treatments = [];

            $('#id_table_treatments tr').each(function() {
                if ($(this).attr('id') != "id_row_treatment") {
                    var col = $(this).find('td').css('backgroundColor');
                    var color = getHexRGBColor(col);
                    if (color == 'ffff99') {
                        var treatment = {};
                        treatment.id = $(this).attr('id');
                        treatment.type = $(this).find(".type").text();
                        treatment.patient = $(this).find(".patient").text();
                        treatment.count = parseInt($(this).find(".count").text());
                        jsonString.treatments.push(treatment);
                    }
                }
            });

            //alert(JSON.stringify(jsonString));

            $.ajax({
                type: 'POST',
                url: 'nurse/save_proc_nurse.html',
                dataType: "text",
                async:false,
                data: JSON.stringify(jsonString),
                contentType: "application/json; charset=utf-8",
                success: function(jsondata) {
                    Callback(jsondata);
                },

                error: function() {
                    alert("Something wrong. :(");
                }
            });

            function Callback(jsondata)
            {
                //alert("Done");
                $('#id_table_treatments tr').each(function() {
                    if ($(this).attr('id') != "id_row_treatment") {
                        $(this).find('td').each (function() {
                            var col = $(this).css('backgroundColor');
                            var color = getHexRGBColor(col);
                            if (color == 'ffff99') {
                                $(this).css('background', '#F8F8F8');
                            }
                        });
                    }
                });

                $('#id_table_tests tr').each(function() {
                    if ($(this).attr('id') != "id_row_test") {
                        $(this).find('td').each (function() {
                            var col = $(this).css('backgroundColor');
                            var color = getHexRGBColor(col);
                            if (color == 'ffff99') {
                                $(this).css('background', '#F8F8F8');
                            }
                        });
                    }
                });
            }
        }
    </script>
    <meta charset="utf-8">

    <style>
        <%@include file='stylesheets/bootstrap.min.css' %>
        <%@include file='stylesheets/default.css' %>
        <%@include file='stylesheets/list.css' %>
        <%@include file='stylesheets/status.css' %>
        <%@include file='stylesheets/feedlist.css' %>
        <%@include file='stylesheets/metro.css' %>
        <%@include file='stylesheets/login.css' %>
        <%@include file='stylesheets/wrap.css' %>
    </style>

    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background: url("${pageContext.request.contextPath}/images/background.jpg");
        }

        .container {
        //width: 800px;
            /*padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #D8BFD8;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 10px;
               -moz-border-radius: 10px;
                    border-radius: 10px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
               -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                    box-shadow: 0 1px 2px rgba(0,0,0,.05);*/
        }

        .simple-little-table {
            font-family:Arial, Helvetica, sans-serif;
            color:#666;
            font-size:12px;
            text-shadow: 1px 1px 0px #fff;
            background:#eaebec;
            margin:20px;
            border:#ccc 1px solid;
            border-collapse:separate;

            -moz-border-radius:3px;
            -webkit-border-radius:3px;
            border-radius:3px;

            -moz-box-shadow: 0 1px 2px #d1d1d1;
            -webkit-box-shadow: 0 1px 2px #d1d1d1;
            box-shadow: 0 1px 2px #d1d1d1;
        }

        .simple-little-table th {
            font-weight:bold;
            padding:21px 25px 22px 25px;
            border-top:1px solid #fafafa;
            border-bottom:1px solid #e0e0e0;

            background: #ededed;
            background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
            background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
        }
        .simple-little-table th:first-child{
            text-align: left;
            padding-left:20px;
        }
        .simple-little-table tr:first-child th:first-child{
            -moz-border-radius-topleft:3px;
            -webkit-border-top-left-radius:3px;
            border-top-left-radius:3px;
        }
        .simple-little-table tr:first-child th:last-child{
            -moz-border-radius-topright:3px;
            -webkit-border-top-right-radius:3px;
            border-top-right-radius:3px;
        }
        .simple-little-table tr{
            text-align: center;
            padding-left:20px;
        }
        .simple-little-table tr td:first-child{
            text-align: left;
            padding-left:20px;
            border-left: 0;
        }
        .simple-little-table tr td {
            padding:18px;
            border-top: 1px solid #ffffff;
            border-bottom:1px solid #e0e0e0;
            border-left: 1px solid #e0e0e0;

            background: #fafafa;
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
        }
        .simple-little-table tr:nth-child(even) td{
            background: #f6f6f6;
            background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
            background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
        }
        .simple-little-table tr:last-child td{
            border-bottom:0;
        }
        .simple-little-table tr:last-child td:first-child{
            -moz-border-radius-bottomleft:3px;
            -webkit-border-bottom-left-radius:3px;
            border-bottom-left-radius:3px;
        }
        .simple-little-table tr:last-child td:last-child{
            -moz-border-radius-bottomright:3px;
            -webkit-border-bottom-right-radius:3px;
            border-bottom-right-radius:3px;
        }
        .simple-little-table tr:hover td{
            background: #f2f2f2;
            background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
            background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);
        }

        .simple-little-table a:link {
            color: #666;
            font-weight: bold;
            text-decoration:none;
        }
        .simple-little-table a:visited {
            color: #999999;
            font-weight:bold;
            text-decoration:none;
        }
        .simple-little-table a:active,
        .simple-little-table a:hover {
            color: #bd5a35;
            text-decoration:underline;
        }

    </style>
</head>
<body>
<div style="margin-right: 21px; margin-top: 0px;" align="right">
		<button class="btn btn-large" type="submit"
                onclick="location.href='/Hospital/j_spring_security_logout.html'"
			>
			Log out
		</button>
	</div>
<div class="simple-little-table" align="center">
    <!--<table style="width: 70%">
        <tr>
            <td>-->
                <div class="simple-little-table" style="float: left;">
                    <table class="table table-bordered" id="id_table_tests">
                        <h4 class="first_column_heading" align="center">Analyses</h4>
                        <tr align="center"  style="height: 50px;" id="id_row_test">
                            <td style="width: 150px;">Type</td>
                            <td style="width: 150px;">Patient</td>
                            <td style="width: 150px;">Result</td>
                        </tr>
                        <c:forEach items="${tests}" var="test">
                            <tr align="center" id="test${test.id}">
                                <td class="type" style="width: 150px;">${fn:escapeXml(test.type)}</td>
                                <td class="patient" style="width: 150px;">${fn:escapeXml(test.patient)}</td>
                                <td class="result" style="width: 150px;"><input class="inresult" placeholder="${fn:escapeXml(test.result)}" style="width: 150px; height: 33px"></td>
                            </tr>
                        </c:forEach>
                    </table>


                </div>
                    <!--</td>
            <td>-->
                <div class="simple-little-table" style="float: left;">
                    <table class="table table-bordered" id="id_table_treatments">
                        <h4 class="first_column_heading" align="center">Procedures</h4>
                        <tr align="center"  style="height: 50px;" id="id_row_treatment">
                            <td style="width: 150px;">Type</td>
                            <td style="width: 150px;">Nurse</td>
                            <td style="width: 150px;">Count</td>
                        </tr>
                        <c:forEach items="${treatments}" var="treatment">
                            <tr align="center" id="treatment${treatment.id}" style="height: 50px; width: 150px">
                                <td class="type" style="width: 150px;">${fn:escapeXml(treatment.type)}</td>
                                <td class="patient" style="width: 150px;">${fn:escapeXml(treatment.patient)}</td>
                                <td style="width: 150px;">
                                    <button class="count" type="submit">${fn:escapeXml(treatment.count)}
                                        </button>
                                    <!-- onclick="countMinusBtn();"-->
                                </td>
                            </tr>
                        </c:forEach>
                    </table>

                <!--<button class="btn btn-large" type="submit" onclick="saveTreatmentBtn();">Save treatment</button>-->
                </div>
                    <!--</td>
        </tr>
    </table>-->

</div>
<div style="clear: both; padding-left:435px">
    <button class="btn btn-large" type="submit"
            style="margin-bottom: 20px; background-color: rgba(112,128,144,0.7)"
            onclick="sendDataToServer();">Save all</button>
</div>
<!--
        <button class="btn btn-large" type="submit" style="margin-bottom: 20px; background-color: rgba(112,128,144,0.7)">Сохранить анализ</button>
        <button class="btn btn-large" type="submit" style="margin-bottom: 20px; background-color: rgba(112,128,144,0.7)">Сохранить процедуру</button>
    -->


</body>
</html>