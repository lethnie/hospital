<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
	<head>
		<title>Sign up</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<link rel="stylesheet" media="screen" href="stylesheets/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="stylesheets/default.css">
        <link rel="stylesheet" media="screen" href="stylesheets/list.css">
        <link rel="stylesheet" media="screen" href="stylesheets/status.css">
        <link rel="stylesheet" media="screen" href="stylesheets/feedlist.css">
        <link rel="stylesheet" media="screen" href="stylesheets/metro.css">
        <link rel="stylesheet" media="screen" href="stylesheets/login.css">
        <link rel="stylesheet" media="screen" href="stylesheets/wrap.css">
		
		<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="${_response_encoding}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                idRoleChange();
                $('#id_role').change(function () {
                    //alert("changed");
                    idRoleChange();
                    return false;
                });
                $('#id_spec').change(function () {
                    var spec = $(this).prop('value');
                    if (spec == -1) {
                        $('#id_cab').empty();
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: 'get_cabinets_by_spec.html',
                            dataType: "text",
                            async:false,
                            data: JSON.stringify({ spec: spec }),
                            contentType: "application/json; charset=utf-8",
                            success: function(jsondata) {

                                Callback(jsondata);
                            },
                            error: function() {
                                alert("Ajax request broken");
                                $('#id_cab').empty();
                            }
                        });

                    }

                    return false;
                });
            });

            function Callback(jsondata)
            {
                //alert(jsondata);
                $('#id_cab').empty();
                var data = $.parseJSON(jsondata);
                $.each(data,function(i,el)
                {
                    $('#id_cab')
                            .append($("<option></option>")
                                    .attr("value",el.key)
                                    .text(el.value));

                });
            }

            function idRoleChange() {
                var role_value = $('#id_role').prop('value');
                //alert("here we are! " + role_value);
                if (role_value == -1) {
                    $('#id_spec').prop('disabled', true).css('color', '#808080');
                    $('#id_cab').prop('disabled', true).css('color', '#808080');
                    $('#id_doc').prop('disabled', true).css('color', '#808080');
                }
                if (role_value == 3) {
                    $('#id_spec').prop('disabled', false).css('color', '#000000');
                    $('#id_cab').prop('disabled', false).css('color', '#000000');
                    $('#id_doc').prop('disabled', true).css('color', '#808080');
                }
                if (role_value == 1) {
                    $('#id_spec').prop('disabled', true).css('color', '#808080');
                    $('#id_cab').prop('disabled', true).css('color', '#808080');
                    $('#id_doc').prop('disabled', true).css('color', '#808080');
                }
                if (role_value == 2) {
                    $('#id_spec').prop('disabled', true).css('color', '#808080');
                    $('#id_cab').prop('disabled', true).css('color', '#808080');
                    $('#id_doc').prop('disabled', true).css('color', '#808080');
                }
                if (role_value == 4) {
                    $('#id_spec').prop('disabled', true).css('color', '#808080');
                    $('#id_cab').prop('disabled', true).css('color', '#808080');
                    $('#id_doc').prop('disabled', false).css('color', '#000000');
                }
            }
        </script>
		
		<meta charset="utf-8">

		 <style>
                <%@include file='stylesheets/bootstrap.min.css' %>
                <%@include file='stylesheets/default.css' %>
                <%@include file='stylesheets/list.css' %>
                <%@include file='stylesheets/status.css' %>
                <%@include file='stylesheets/feedlist.css' %>
                <%@include file='stylesheets/metro.css' %>
                <%@include file='stylesheets/login.css' %>
                <%@include file='stylesheets/wrap.css' %>
            </style>
		
		<style type="text/css">
			body {
				padding-top: 40px;
				padding-bottom: 40px;
				background: url("${pageContext.request.contextPath}/images/background.jpg");
			  }

			  .form-signup {
				max-width: 500px;
				padding: 19px 29px 29px;
				margin: 0 auto 20px;
				background-color: rgba(211,211,211,0.8);
				border: 1px solid #e5e5e5;
				-webkit-border-radius: 10px;
				   -moz-border-radius: 10px;
						border-radius: 10px;
				-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
				   -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
						box-shadow: 0 1px 2px rgba(0,0,0,.05);
			  }

			  
			  .form-signup .form-signup-heading
			  {
				margin-bottom: 10px;
				text-align: center;
			  }
			 
			  .name_container,
			  .password_container,
			  .type_container,
			  .department_container,
			  .room_container,
			  .doctor_container
 			  {
				font-size: 16px;
				margin-bottom: 5px;
				margin-right: 20%;
				padding: 2px 5px;
				
		      }
			  .form-signup button[type="submit"] {
				background-color: #708090;
			  }
		</style>
	</head>
	<body>
	<div class="container" >

      <form:form class="form-signup" align="right" method="post" action = "registration/signup.html" commandName="user">
        <h2 class="form-signup-heading">Please sign up</h2>
		<div class = "name_container">
		<p> Name
        <form:input type="text" class="input-block-level" path="name" placeholder="Name" style="width: 200px; height: 35px; border-radius: 5px;"/> </p> </div>
		<div class = "password_container">
		<p> Password
        <form:input type="password" class="input-block-level" path="password" placeholder="Password" style="width: 200px; height: 35px; border-radius: 5px;"/></p> </div>
		<div class = "type_container">
		<p> Type
        <form:select type="roles" id="id_role" path="role" style="width: 200px; height: 35px; border-radius: 5px;">
			<form:options items='${rolesList}'/>
		</form:select></p> </div>
		<div class = "department_container">
			<p> Specialization
				<form:select name="departments" id="id_spec" path="specialization" style="width: 200px; height: 35px; border-radius: 5px; color: #808080;">
                    <form:options items='${specializationsList}'/>
				</form:select>
			</p> 
		</div>
		<div class = "room_container">
		<p> Cabinet
            <form:select name="cabinets" id="id_cab" path="cabinet" style="width: 200px; height: 35px; border-radius: 5px; color: #808080;">
                <form:options items='${cabinetsList}'/>
            </form:select></p> </div>
		<div class = "doctor_container">
		<p> Doctor
        <form:select name="doctors" id="id_doc" path="doctor" style="width: 200px; height: 35px; border-radius: 5px; color: #808080;">
            <form:options items='${doctorsList}'/>
		</form:select></p> </div>
          <br>
          <font color="FF0000">
                  ${reg_status}
          </font>
          <br>
		<button class="btn btn-large" type="submit">Sign up</button>
      </form:form>

    </div> 
	</body>
</html>