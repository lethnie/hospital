<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/default.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/list.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/status.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/feedlist.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/metro.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/login.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/wrap.css">

    <script src="${pageContext.request.contextPath}/javascripts/bootstrap.min.js"></script>

    <!--<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>-->

    <meta charset="utf-8">

    <style>
    <%@include file='stylesheets/bootstrap.min.css' %>
    <%@include file='stylesheets/default.css' %>
    <%@include file='stylesheets/list.css' %>
    <%@include file='stylesheets/status.css' %>
    <%@include file='stylesheets/feedlist.css' %>
    <%@include file='stylesheets/metro.css' %>
    <%@include file='stylesheets/login.css' %>
    <%@include file='stylesheets/wrap.css' %>
    </style>

    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background: url("${pageContext.request.contextPath}/images/background.jpg");
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: rgba(211,211,211,0.8);
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"],
        .form-signin select[name="roles"]
        {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
            border-radius: 5px;
        }
        .form-signin button[type="submit"] {
            background-color: #708090;
        }
    </style>
</head>
<body>
<div class="container" align="center">

    <form class="form-signin" method="post"  action="<c:url value='j_spring_security_check.html' />">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" name = "j_username" class="input-block-level" placeholder="Name"/> <br>
        <input type="password" name="j_password" class="input-block-level" placeholder="Password"/> <br>
        <br>
        <font color="FF0000">
            ${auth_status}
        </font>
        <br>
        <br>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
    </form>

</div>
</body>
</html>