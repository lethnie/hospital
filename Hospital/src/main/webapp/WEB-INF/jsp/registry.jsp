<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Patient_analyses</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/default.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/list.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/status.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/feedlist.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/metro.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/login.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/wrap.css">

    <script src="${pageContext.request.contextPath}/javascripts/bootstrap.min.js"></script>

    <!--<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>-->
    <script type="text/javascript">

        $(document).ready(function(){
            $('#id_spec').change(function () {

                var spec = $(this).prop('value');
                //alert("here we are! " + spec);
                if (spec == -1) {
                    $('#id_doc').empty();
                } else {
                    $.ajax({
                        type: 'POST',
                        //dataType: 'json',
                        url: 'registry/get_doctors_by_specialization.html',
                        dataType: "text",
                        async:false,
                        data: JSON.stringify({ spec: spec }),
                        contentType: "application/json; charset=utf-8",
                        success: function(jsondata) {

                            Callback(jsondata);
                        },

                        error: function() {
                            //alert("Ajax request broken");
                            $('#id_doc').empty();
                        }
                    });

                }

                return false;
            });

            $('#id_doc').change(function () {
                //alert("here we are!");
                var doc = $(this).prop('value');

                if (doc == -1) {
                    //$('#id_doc').empty();
                    $('#id_schedule').empty();
                    $('#id_schedule')
                            .append($("<tr></tr>")
                                    .attr("align", "center")
                                    .append($("<td></td>")
                                            .attr("align", "left")
                                            .attr("style", "height: 50px")
                                            .text("Day"))
                                    .append($("<td></td>")
                                            .attr("align", "left")
                                            .text("Time"))
                            );
                } else {
                    $.ajax({
                        type: 'POST',
                        //dataType: 'json',
                        url: 'registry/get_doctor_schedule_by_id.html',
                        dataType: "text",
                        async:false,
                        data: JSON.stringify({ doc: doc }),
                        contentType: "application/json; charset=utf-8",
                        success: function(jsondata) {

                            CallbackSchedule(jsondata);
                        },

                        error: function() {
                            //alert("Ajax request broken");
                            //$('#id_doc').empty();
                            $('#id_schedule').empty();
                            $('#id_schedule')
                                    .append($("<tr></tr>")
                                            .attr("align", "center")
                                            .attr("style", "height: 50px")
                                            .append($("<td></td>")
                                                    .attr("align", "left")
                                                    .text("Day"))
                                            .append($("<td></td>")
                                                    .attr("align", "left")
                                                    .text("Time"))
                                    );
                        }
                    });

                }

                return false;
            });
        });

        function Callback(jsondata)
        {
            //alert(jsondata);
            $('#id_doc').empty();
            var data = $.parseJSON(jsondata);
            $.each(data,function(i,el)
            {
                $('#id_doc')
                        .append($("<option></option>")
                                .attr("value",el.key)
                                .text(el.value));
            });
        }

        function CallbackSchedule(jsondata)
        {
            //alert(jsondata);
            //$('#id_doc').empty();
            $('#id_schedule').empty();
            $('#id_schedule')
                    .append($("<tr></tr>")
                            .attr("align", "center")
                            .attr("style", "height: 50px")
                            .append($("<td></td>")
                                    .attr("align", "left")
                                    .text("Day"))
                            .append($("<td></td>")
                                    .attr("align", "left")
                                    .text("Time"))
                    );

            var data = $.parseJSON(jsondata);
            $.each(data,function(i,el)
            {
                $('#id_schedule')
                        .append($("<tr></tr>")
                                .attr("align", "center")
                                .append($("<td></td>")
                                        .attr("align", "left")
                                        .text(el.key))
                                .append($("<td></td>")
                                        .attr("align", "left")
                                        .text(el.value))
                        );

            });
        }
    </script>

    <meta charset="utf-8">

    <style>
        <%@include file='stylesheets/bootstrap.min.css' %>
        <%@include file='stylesheets/default.css' %>
        <%@include file='stylesheets/list.css' %>
        <%@include file='stylesheets/status.css' %>
        <%@include file='stylesheets/feedlist.css' %>
        <%@include file='stylesheets/metro.css' %>
        <%@include file='stylesheets/login.css' %>
        <%@include file='stylesheets/wrap.css' %>
    </style>
    <style type="text/css">

        body {
            background: url("${pageContext.request.contextPath}/images/background.jpg");
        }

        .container {
            max-width: 800px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #D8BFD8;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }

        .simple-little-table {
            font-family:Arial, Helvetica, sans-serif;
            color:#666;
            font-size:12px;
            text-shadow: 1px 1px 0px #fff;
            background:#eaebec;
            margin:20px;
            border:#ccc 1px solid;
            border-collapse:separate;

            -moz-border-radius:3px;
            -webkit-border-radius:3px;
            border-radius:3px;

            -moz-box-shadow: 0 1px 2px #d1d1d1;
            -webkit-box-shadow: 0 1px 2px #d1d1d1;
            box-shadow: 0 1px 2px #d1d1d1;
        }

        .simple-little-table th {
            font-weight:bold;
            padding:21px 25px 22px 25px;
            border-top:1px solid #fafafa;
            border-bottom:1px solid #e0e0e0;

            background: #ededed;
            background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
            background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
        }
        .simple-little-table th:first-child{
            text-align: left;
            padding-left:20px;
        }
        .simple-little-table tr:first-child th:first-child{
            -moz-border-radius-topleft:3px;
            -webkit-border-top-left-radius:3px;
            border-top-left-radius:3px;
        }
        .simple-little-table tr:first-child th:last-child{
            -moz-border-radius-topright:3px;
            -webkit-border-top-right-radius:3px;
            border-top-right-radius:3px;
        }
        .simple-little-table tr{
            text-align: center;
            padding-left:20px;
        }
        .simple-little-table tr td:first-child{
            text-align: left;
            padding-left:20px;
            border-left: 0;
        }
        .simple-little-table tr td {
            padding:18px;
            border-top: 1px solid #ffffff;
            border-bottom:1px solid #e0e0e0;
            border-left: 1px solid #e0e0e0;

            background: #fafafa;
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
        }
        .simple-little-table tr:nth-child(even) td{
            background: #f6f6f6;
            background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
            background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
        }
        .simple-little-table tr:last-child td{
            border-bottom:0;
        }
        .simple-little-table tr:last-child td:first-child{
            -moz-border-radius-bottomleft:3px;
            -webkit-border-bottom-left-radius:3px;
            border-bottom-left-radius:3px;
        }
        .simple-little-table tr:last-child td:last-child{
            -moz-border-radius-bottomright:3px;
            -webkit-border-bottom-right-radius:3px;
            border-bottom-right-radius:3px;
        }
        .simple-little-table tr:hover td{
            background: #f2f2f2;
            background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
            background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);
        }

        .simple-little-table a:link {
            color: #666;
            font-weight: bold;
            text-decoration:none;
        }
        .simple-little-table a:visited {
            color: #999999;
            font-weight:bold;
            text-decoration:none;
        }
        .simple-little-table a:active,
        .simple-little-table a:hover {
            color: #bd5a35;
            text-decoration:underline;
        }

    </style>
</head>
<!--width: initial; height: initial;-->
<body>
<nav class="navbar navbar-default" role="navigation" style="background-color: rgba(112,128,144,0.7);">
    <div class="container-fluid">
    <button class="btn btn-large" type="submit"
            onclick="location.href='/Hospital/j_spring_security_logout.html'"
            style="margin: 7px 20px 0 20px; display: inline;float: right;">
    				Log out
    			</button>
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Menu</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Hospital/registry/registration.html">Registration</a></li>
                <li><a href="/Hospital/registry/appointments/new.html">New appointment</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class = "type_container" align="center">
    <p> Choose the department <br>
        <select type="roles" id="id_spec" style="width: 400px; height: 35px">
            <!--<option value="patient">Surgery</option>-->
            <c:forEach items="${specList}" var="spec">
                <option value="${spec.key}">${spec.value}</option>
            </c:forEach>
        </select></p> </div>
<div class = "type_container" align="center">
    <p> Choose the doctor </br>
        <select type="roles" id="id_doc" style="width: 400px; height: 35px">
            <!--<option value="patient">Bla</option>-->
        </select>
    </p>
</div>
<div class="simple-little-table" align="center">
    <h4 class="first_column_heading">Doctors</h4>
    <table class="table table-bordered" id="id_schedule">
        <tr align="center"  style="height: 50px">
            <td align="left">Day</td>
            <td align="left">Time</td>
        </tr>
        <!--<tr align="center">
            <td align="left">content1</td>
            <td align="left">content2</td>
        </tr>-->
    </table>
</div>
</body>
</html>