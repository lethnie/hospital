<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Приёмы</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/default.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/list.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/status.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/feedlist.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/metro.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/login.css">
    <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/stylesheets/wrap.css">

    <script src="${pageContext.request.contextPath}/javascripts/bootstrap.min.js"></script>

    <!--<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>-->

    <script type="text/javascript">
        $(document).ready(function () {

            /*function btnclick() {
                alert("wtf?");
            }*/
            $('#id_table tr').click(function (event) {
                //alert("hello");
                var id = $(this).attr('id');
                //alert(id);
                if (id != "id_tr")
                    window.location.href = 'doctor/reception.html?id='.concat(id);
            });

            /*$('tr').click(function (event) {
                alert("hellohello");
                var id = $(this).attr('id');
                alert(id);
                if (id != "id_tr")
                    window.location.href = 'reception.html?id='.concat(id);
            });*/
        });
    </script>
    <meta charset="utf-8">

    <style>
        <%@include file='stylesheets/bootstrap.min.css' %>
        <%@include file='stylesheets/default.css' %>
        <%@include file='stylesheets/list.css' %>
        <%@include file='stylesheets/status.css' %>
        <%@include file='stylesheets/feedlist.css' %>
        <%@include file='stylesheets/metro.css' %>
        <%@include file='stylesheets/login.css' %>
        <%@include file='stylesheets/wrap.css' %>
    </style>

    <style type="text/css">
        body {
        //padding-top: 40px;
            padding-bottom: 40px;
            background: url("${pageContext.request.contextPath}/images/background.jpg");
            text-align:center;
        }

        .simple-little-table {
            font-family:Arial, Helvetica, sans-serif;
            color:#666;
            font-size:12px;
            text-shadow: 1px 1px 0px #fff;
            background:#eaebec;
            margin:20px;
            border:#ccc 1px solid;
            border-collapse:separate;

            -moz-border-radius:3px;
            -webkit-border-radius:3px;
            border-radius:3px;

            -moz-box-shadow: 0 1px 2px #d1d1d1;
            -webkit-box-shadow: 0 1px 2px #d1d1d1;
            box-shadow: 0 1px 2px #d1d1d1;
        }

        .simple-little-table th {
            font-weight:bold;
            padding:21px 25px 22px 25px;
            border-top:1px solid #fafafa;
            border-bottom:1px solid #e0e0e0;

            background: #ededed;
            background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
            background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
        }
        .simple-little-table th:first-child{
            text-align: left;
            padding-left:20px;
        }
        .simple-little-table tr:first-child th:first-child{
            -moz-border-radius-topleft:3px;
            -webkit-border-top-left-radius:3px;
            border-top-left-radius:3px;
        }
        .simple-little-table tr:first-child th:last-child{
            -moz-border-radius-topright:3px;
            -webkit-border-top-right-radius:3px;
            border-top-right-radius:3px;
        }
        .simple-little-table tr{
            text-align: center;
            padding-left:20px;
        }
        .simple-little-table tr td:first-child{
            text-align: left;
            padding-left:20px;
            border-left: 0;
        }
        .simple-little-table tr td {
            padding:18px;
            border-top: 1px solid #ffffff;
            border-bottom:1px solid #e0e0e0;
            border-left: 1px solid #e0e0e0;

            background: #fafafa;
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
        }
        .simple-little-table tr:nth-child(even) td{
            background: #f6f6f6;
            background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
            background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
        }
        .simple-little-table tr:last-child td{
            border-bottom:0;
        }
        .simple-little-table tr:last-child td:first-child{
            -moz-border-radius-bottomleft:3px;
            -webkit-border-bottom-left-radius:3px;
            border-bottom-left-radius:3px;
        }
        .simple-little-table tr:last-child td:last-child{
            -moz-border-radius-bottomright:3px;
            -webkit-border-bottom-right-radius:3px;
            border-bottom-right-radius:3px;
        }
        .simple-little-table tr:hover td{
            background: #f2f2f2;
            background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
            background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);
        }

        .simple-little-table a:link {
            color: #666;
            font-weight: bold;
            text-decoration:none;
        }
        .simple-little-table a:visited {
            color: #999999;
            font-weight:bold;
            text-decoration:none;
        }
        .simple-little-table a:active,
        .simple-little-table a:hover {
            color: #bd5a35;
            text-decoration:underline;
        }


        button[type="submit"] {
            width: 30%;
            height: 5%;
            background-color: rgba(112,128,144,0.9);
        }


    </style>
</head>
<body>
<!--<form action="doctor/reception.html">-->
<div style="margin: 21px;" align="right">
		<button class="btn btn-large" type="submit"
                style="width: 100px"
                onclick="location.href='/Hospital/j_spring_security_logout.html'"
			    >
			Log out
		</button>
	</div>
<div class="simple-little-table" align="center">
    <table class="table table-bordered" id="id_table">
        <h4 class="first_column_heading">Receptions</h4>
        <tr align="center"  style="height: 50px" id="id_tr">
            <td>Time</td>
            <td>Patient</td>
        </tr>
        <c:forEach items="${receptions}" var="reception">
            <tr align="center" id="${reception.id}">
                <td>${fn:escapeXml(reception.time)}</td>
                <td>${fn:escapeXml(reception.patient)}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<!--<button class="btn btn-large" type="submit" onclick="btnclick();">Host receprion</button>-->
<!--</form>-->
</body>
</html>