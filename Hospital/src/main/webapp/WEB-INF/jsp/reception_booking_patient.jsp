<%@ page import="ru.vsu.cs.hospital.service.ScheduleTimeService" %>
<%@ page import="org.springframework.beans.factory.annotation.Autowired" %>
<%@ page import="ru.vsu.cs.hospital.service.ScheduleTimeServiceImpl" %>
<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 30.04.14
  Time: 0:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Reception</title>
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" media="screen" href="stylesheets/bootstrap.min.css">
    <link rel="stylesheet" media="screen" href="stylesheets/default.css">
    <link rel="stylesheet" media="screen" href="stylesheets/list.css">
    <link rel="stylesheet" media="screen" href="stylesheets/status.css">
    <link rel="stylesheet" media="screen" href="stylesheets/feedlist.css">
    <link rel="stylesheet" media="screen" href="stylesheets/metro.css">
    <link rel="stylesheet" media="screen" href="stylesheets/login.css">
    <link rel="stylesheet" media="screen" href="stylesheets/wrap.css">

    <script src="javascripts/bootstrap.min.js" type="text/javascript" charset="${_response_encoding}"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#id_day').change(function () {
            //alert("here we are!");
            var day = $(this).prop('value');
            var doc = $('#id_doc').prop('value');
            if ((day == -1) || (doc == -1)) {
                $('#id_time').empty();
            } else {


                /*var values = day + "%" + doc;
                alert("values! " + values);*/
                $.ajax({
                    type: 'POST',
                    //dataType: 'json',
                    url: 'get_time.html',
                    dataType: "text",
                    async:false,
                    data: JSON.stringify({ day: day, doc: doc }),
                    contentType: "application/json; charset=utf-8",
                    success: function(jsondata) {

                         Callback(jsondata);
                    },

                    error: function() {
                        //alert("Ajax request broken");
                        $('#id_time').empty();
                    }
                });

            }

            return false;
        });
        $('#id_doc').change(function () {
            //alert("here we are!");
            var doc = $(this).prop('value');
            var day = $('#id_day').prop('value');
            if ((doc == -1) || (day == -1)) {
                $('#id_time').empty();
            } else {


                /*var values = day + "%" + doc;
                 alert("values! " + values);*/
                $.ajax({
                    type: 'POST',
                    //dataType: 'json',
                    url: 'get_time.html',
                    dataType: "text",
                    async:false,
                    data: JSON.stringify({ day: day, doc: doc }),
                    contentType: "application/json; charset=utf-8",
                    success: function(jsondata) {

                        Callback(jsondata);
                    },

                    error: function() {
                        alert("Ajax request broken");
                        $('#id_time').empty();
                    }
                });

            }

            return false;
        });
    });

    function Callback(jsondata)
    {
        //alert(jsondata);
        $('#id_time').empty();
        var data = $.parseJSON(jsondata);
        $.each(data,function(i,el)
        {
            $('#id_time')
                    .append($("<option></option>")
                    .attr("value",el.key)
                    .text(el.value));

        });
    }
    </script>

    <meta charset="utf-8">

    <style>
        <%@include file='stylesheets/bootstrap.min.css' %>
        <%@include file='stylesheets/default.css' %>
        <%@include file='stylesheets/list.css' %>
        <%@include file='stylesheets/status.css' %>
        <%@include file='stylesheets/feedlist.css' %>
        <%@include file='stylesheets/metro.css' %>
        <%@include file='stylesheets/login.css' %>
        <%@include file='stylesheets/wrap.css' %>
    </style>

    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background: url("${pageContext.request.contextPath}/images/background.jpg");
        }

        .container {
            background-color:  rgba(105,105,105,0.6	);
            width: 100%;
        }

        .form-signup {
            padding: 19px 29px 29px;
        }


        .form-signup .form-signup-heading
        {
            margin-bottom: 10px;
            text-align: center;
        }

        .type_container,
        .time_container,
        .date_container,
        .doctor_container
        {
            font-size: 16px;
            margin-bottom: 5px;
            margin-right: 35%;
            padding: 2px 5px;
        }
        .form-signup button[type="submit"] {
            background-color: #708090;
        }
    </style>
</head>
<body>
<div class="container" >

    <form:form class="form-signup" align="right" commandName="app" method="get" action="add.html">
        <h3 class="form-signup-heading">Appointment</h3>

        <div class = "type_container">
            <p> Patient
                <form:select type="roles" path="patient" id="id_pat" style="width: 300px; height: 35px">
                       <form:options items="${patientsList}" />
                </form:select></p> </div>

        <div class = "doctor_container">
            <p> Doctor
                <form:select name="doctors" id="id_doc"
                             path="doctor"
                             style="width: 300px; height: 35px">

                    <form:options items='${doctorsList}'/>
                </form:select></p> </div>

        <div class = "date_container">
            <p> Date
        <form:select name="days" path="day" id="id_day" style="width: 300px; height: 35px;">
            <form:options items="${daysList}" />

        </form:select>
            </p>
            </div>
        <div class = "time_container">
            <p> Time
                <form:select type="time" id="id_time" path="start" style="width: 300px; height: 35px">

                </form:select></p> </div>
        <br>
        <div class = "type_container">
        <font color="FF0000">
                ${incorrect}
        </font>
        </div>
        <br>
        <button class="btn btn-large" type="submit"
                style="width: 200px; height: 35px; margin-right: 40.5%;">Make appointment</button>
    </form:form>

</div>
</body>
</html>

<!--<div class = "date_container">
<p> Дата
<input type="date" style="width: 300px; height: 35px"> </p> </div>-->