<%--
  Created by IntelliJ IDEA.
  User: 123
  Date: 23.04.14
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
	<head>
		<title>Reception</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<link rel="stylesheet" media="screen" href="stylesheets/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="stylesheets/default.css">
        <link rel="stylesheet" media="screen" href="stylesheets/list.css">
        <link rel="stylesheet" media="screen" href="stylesheets/status.css">
        <link rel="stylesheet" media="screen" href="stylesheets/feedlist.css">
        <link rel="stylesheet" media="screen" href="stylesheets/metro.css">
        <link rel="stylesheet" media="screen" href="stylesheets/login.css">
        <link rel="stylesheet" media="screen" href="stylesheets/wrap.css">
		
		<script src="javascripts/bootstrap.min.js" type="text/javascript" charset="${_response_encoding}"></script>
        <script src="javascripts/json2.js"></script>

        <script>
            $(document).ready(function(){
                $("#id_add_test").hide();
                $("#id_add_treatment").hide();
            });

            function addTestBtn() {
                $("#id_add_test").show();
                $("#id_add_treatment").hide();
            }

            function addTest() {
                var test = $("#id_test_name").prop('value');
                var testText = $("#id_test_name option:selected").text();
                var nurse = $("#id_test_nurse").prop('value');
                var nurseText = $("#id_test_nurse option:selected").text();
                //alert("TEST: " + test + ", TEXT: " + testText + ", NURSE: " + nurse);
                $("#id_table_tests").append($("<tr></tr>")
                        .attr("align", "center")
                        .attr("id", test)
                        .append($("<td></td>")
                                .attr("class", "type")
                                .text(testText))
                        .append($("<td></td>")
                                .attr("class", "nurse")
                                .text(nurseText))
                );
                $("#id_add_test").hide();
            }

            function addTreatmentBtn() {
                $("#id_add_treatment").show();
                $("#id_add_test").hide();
            }

            function addTreatment() {
                var treatment = $("#id_treatment_name").prop('value');
                var treatmentText = $("#id_treatment_name option:selected").text();
                var nurse = $("#id_treatment_nurse").prop('value');
                var nurseText = $("#id_treatment_nurse option:selected").text();
                var count = $("#id_treatment_count").prop('value');
                //alert("TEST: " + test + ", TEXT: " + testText + ", NURSE: " + nurse);
                $("#id_table_treatments").append($("<tr></tr>")
                        .attr("align", "center")
                        .attr("id", treatment)
                        .append($("<td></td>")
                                .attr("class", "type")
                                .text(treatmentText))
                        .append($("<td></td>")
                                .attr("class", "nurse")
                                .text(nurseText))
                        .append($("<td></td>")
                                .attr("class", "count")
                                .text(count))
                );
                $("#id_add_treatment").hide();
            }

            function sendDataToServer() {
                var jsonString = {};
                jsonString.diagnosis = $('#id_diagnosis').val();
                jsonString.patient = $('#id_label_patient').text();
                jsonString.app = $('#id_label_app').text();
                //alert(JSON.stringify(jsonString));
                jsonString.tests = [];

                $('#id_table_tests tr').each(function() {
                    if ($(this).attr('id') != "id_row_test") {
                        var test = {};
                        test.id = $(this).attr('id');
                        test.type = $(this).find(".type").text();
                        test.nurse = $(this).find(".nurse").text();
                        jsonString.tests.push(test);
                    }
                });

                jsonString.treatments = [];

                $('#id_table_treatments tr').each(function() {
                    if ($(this).attr('id') != "id_row_treatment") {
                        var treatment = {};
                        treatment.id = $(this).attr('id');
                        treatment.type = $(this).find(".type").text();
                        treatment.nurse = $(this).find(".nurse").text();
                        treatment.count = $(this).find(".count").text();
                        jsonString.treatments.push(treatment);
                    }
                });

                //alert(JSON.stringify(jsonString));

                $.ajax({
                    type: 'POST',
                    url: 'save_app.html',
                    dataType: "text",
                    async:false,
                    data: JSON.stringify(jsonString),
                    contentType: "application/json; charset=utf-8",
                    success: function(jsondata) {
                        Callback(jsondata);
                    },

                    error: function() {
                        alert("Something wrong. :(");
                    }
                });

                function Callback(jsondata)
                {
                    window.location.href = '/Hospital/doctor.html';
                }
            }
        </script>
		<meta charset="utf-8">

		    <style>
                <%@include file='stylesheets/bootstrap.min.css' %>
                <%@include file='stylesheets/default.css' %>
                <%@include file='stylesheets/list.css' %>
                <%@include file='stylesheets/status.css' %>
                <%@include file='stylesheets/feedlist.css' %>
                <%@include file='stylesheets/metro.css' %>
                <%@include file='stylesheets/login.css' %>
                <%@include file='stylesheets/wrap.css' %>
            </style>
		
		<style type="text/css">
			body {
				padding-top: 40px;
				padding-bottom: 40px;
				background: url("${pageContext.request.contextPath}/images/background.jpg");
			  }
			  
			  .container {
				background-color:  rgba(105,105,105,0.6	);
				width: 100%;
			  }

			  .form-signup {
				padding: 19px 29px 29px;
			  }

			  
			  .form-signup .form-signup-heading
			  {
				margin-bottom: 10px;
				text-align: center;
			  }
			 
			  .patient_container,
			  .diagnos_container,
			  .analysis_container,
			  .btn btn-large,
			  .procedures_container,
			  .doctor_container
 			  {
				font-size: 16px;
				margin-bottom: 5px;
				margin-right: 20%;
				margin-left:15px;
				padding: 2px 5px;
		      }
			  
			  button[type="submit"] {
				background-color: rgba(112,128,144,0.9);
				margin-left: 15px;
				margin-top:15px;
			  }
			  
			  
			  .simple-little-table {
				font-family:Arial, Helvetica, sans-serif;
				color:#666;
				font-size:12px;
				text-shadow: 1px 1px 0px #fff;
				background:#eaebec;
				margin-top:20px;
				margin-left:15px;
				border:#ccc 1px solid;
				border-collapse:separate;
			 
				-moz-border-radius:3px;
				-webkit-border-radius:3px;
				border-radius:3px;
			 
				-moz-box-shadow: 0 1px 2px #d1d1d1;
				-webkit-box-shadow: 0 1px 2px #d1d1d1;
				box-shadow: 0 1px 2px #d1d1d1;
			}
			 
			.simple-little-table th {
				font-weight:bold;
				padding:21px 25px 22px 25px;
				border-top:1px solid #fafafa;
				border-bottom:1px solid #e0e0e0;

			}
			.simple-little-table th:first-child{
				text-align: left;
				padding-left:20px;
			}
			.simple-little-table tr:first-child th:first-child{
				-moz-border-radius-topleft:3px;
				-webkit-border-top-left-radius:3px;
				border-top-left-radius:3px;
			}
			.simple-little-table tr:first-child th:last-child{
				-moz-border-radius-topright:3px;
				-webkit-border-top-right-radius:3px;
				border-top-right-radius:3px;
			}
			.simple-little-table tr{
				text-align: center;
				padding-left:20px;
			}
			.simple-little-table tr td:first-child{
				text-align: left;
				padding-left:20px;
				border-left: 0;
			}
			.simple-little-table tr td {
				padding:18px;
				border-top: 1px solid #ffffff;
				border-bottom:1px solid #e0e0e0;
				border-left: 1px solid #e0e0e0;
				
				background: #fafafa;
				background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
				background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
			}
	
			.simple-little-table tr:last-child td{
				border-bottom:0;
			}
			.simple-little-table tr:last-child td:first-child{
				-moz-border-radius-bottomleft:3px;
				-webkit-border-bottom-left-radius:3px;
				border-bottom-left-radius:3px;
			}
			.simple-little-table tr:last-child td:last-child{
				-moz-border-radius-bottomright:3px;
				-webkit-border-bottom-right-radius:3px;
				border-bottom-right-radius:3px;
			}
			 
			.simple-little-table a:link {
				color: #666;
				font-weight: bold;
				text-decoration:none;
			}
			.simple-little-table a:visited {
				color: #999999;
				font-weight:bold;
				text-decoration:none;
			}
			.simple-little-table a:active,
			.simple-little-table a:hover {
				color: #bd5a35;
				text-decoration:underline;
			}
		</style>
	</head>
	<body>

	<div class="container" >
	<!--<button class="btn btn-large" type="submit"
    			style="background-color: rgba(128,0,0,0.6); width: initial; height: initial; margin: 21px; float:right;">
    			Log out
    		</button>-->
    <button class="btn btn-large" type="submit"
            onclick="location.href='/Hospital/j_spring_security_logout.html'"
                style="margin: 21px; float:right;">
                Log out
            </button>
      <!--<form class="form-signup" method="get">-->
        <h3 class="form-signup-heading">Reception</h3>
		<div class = "patient_container">
		<p> Patient -
            <label id="id_label_patient">${patient}</label>
        <!--<select name="patients" style="width: 300px; height: 35px">
			<option value="1">Pashkin</option>
		</select>--></p> </div>
		<div class = "diagnos_container">
			<p> Diagnosis
			<input type="text" id="id_diagnosis" class="input-block-level" placeholder="Diagnosis" style="width: 300px; height: 35px;"/> </p> </div>
		<table style="width: 70%">
			<tr>
				<td>
					<div class="simple-little-table">
						<table class="table table-bordered" id="id_table_tests">
							<h4 class="first_column_heading" align="center">Analyses</h4>
							<tr align="center"  style="height: 50px" id="id_row_test">
								<td>Type</td>
								<td>Nurse</td>
							</tr>
                            <c:forEach items="${tests}" var="test">
                                <tr align="center" id="${test.id}">
                                    <td class="type">${fn:escapeXml(test.type)}</td>
                                    <td class="nurse">${fn:escapeXml(test.nurse)}</td>
                                </tr>
                            </c:forEach>
						</table>
					</div>
                    <button class="btn btn-large" type="submit" onclick="addTestBtn();">Add analyse</button>
				</td>
				<td>
					<div class="simple-little-table">
						<table class="table table-bordered" id="id_table_treatments">
							<h4 class="first_column_heading" align="center">Procedures</h4>
							<tr align="center"  style="height: 50px" id="id_row_treatment">
								<td>Type</td>
								<td>Nurse</td>
                                <td>Count</td>
							</tr>
                            <c:forEach items="${treatments}" var="treatment">
                            <tr align="center" id="${treatment.id}">
                                <td class="type">${fn:escapeXml(treatment.type)}</td>
                                <td class="nurse">${fn:escapeXml(treatment.nurse)}</td>
                                <td class="count">${fn:escapeXml(treatment.count)}</td>
                            </tr>
                            </c:forEach>
						</table>
					</div>
					<button class="btn btn-large" type="submit" onclick="addTreatmentBtn();">Add procedure</button>
				</td>
			</tr>
		</table>

          <div id="id_add_test">
          <div class = "type_container">
              <p> Name
                  <select id="id_test_name" style="width: 300px; height: 35px">
                      <c:forEach items="${all_tests}" var="some_test">
                          <option value="${fn:escapeXml(some_test.id)}">${fn:escapeXml(some_test.test)}</option>
                      </c:forEach>
                  </select></p> </div>
          <div class = "type_container">
              <p> Nurse
                  <select id="id_test_nurse" style="width: 300px; height: 35px">
                      <c:forEach items="${nurses}" var="nurse">
                          <option value="${fn:escapeXml(nurse.id)}">${fn:escapeXml(nurse.name)}</option>
                      </c:forEach>
                  </select></p> </div>
          <button class="btn btn-large" type="submit" style="width: 200px; height: 35px; margin-right: 40.5%;" onclick="addTest();">Add</button>
      </div>

          <div id="id_add_treatment">
              <div class = "type_container">
                  <p> Name
                      <select id="id_treatment_name" style="width: 300px; height: 35px">
                          <c:forEach items="${all_treatments}" var="some_treatment">
                              <option value="${fn:escapeXml(some_treatment.id)}">${fn:escapeXml(some_treatment.treatment)}</option>
                          </c:forEach>
                      </select></p> </div>
              <div class = "type_container">
                  <p> Nurse
                      <select id="id_treatment_nurse" style="width: 300px; height: 35px">
                          <c:forEach items="${nurses}" var="nurse">
                              <option value="${fn:escapeXml(nurse.id)}">${fn:escapeXml(nurse.name)}</option>
                          </c:forEach>
                      </select></p> </div>
              <div class = "type_container">
                  <p> Count
                      <input type="text" id="id_treatment_count" class="input-block-level" placeholder="1" style="width: 300px; height: 35px;"/>
                  </p></div>
              <button class="btn btn-large" type="submit" style="width: 200px; height: 35px; margin-right: 40.5%;" onclick="addTreatment();">Add</button>
          </div>

		<button class="btn btn-large" type="submit" style="width: 10%; margin-top: 30px" onclick="sendDataToServer();">Ok</button>
      <!--</form>-->
	 
    </div>
    <label id="id_label_app" style="visibility: hidden">${id_app}</label>
	</body>
</html>