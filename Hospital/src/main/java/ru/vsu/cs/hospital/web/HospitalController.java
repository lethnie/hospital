package ru.vsu.cs.hospital.web;

import org.joda.time.DateMidnight;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.vsu.cs.hospital.domain.*;
import ru.vsu.cs.hospital.dto.*;
import ru.vsu.cs.hospital.service.*;

import java.util.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 20:41
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class HospitalController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private ReferralService referralService;

    @Autowired
    private PrescriptionService prescriptionService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ScheduleTimeService scheduleTimeService;

    @Autowired
    private SpecializationService specializationService;

    @Autowired
    private CabinetService cabinetService;

    @Autowired
    private TestService testService;

    @Autowired
    private TreatmentService treatmentService;

    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public String index(Model model, @RequestParam(required=false) String auth_status) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!auth.isAuthenticated()) {
            model.addAttribute("auth_status", auth_status);
            return "index";
        }
        else {
            if ( auth.getPrincipal().equals("guest")) {
                model.addAttribute("auth_status", auth_status);
                return "index";
            }
            User us = userService.findUserByName(((org.springframework.security.core.userdetails.User)auth.getPrincipal()).getUsername());
            String role = us.getRole().getRole();
            if (role.equals("patient")) {
                return getReceptions(model);//"patient_receptions";
            }
            if (role.equals("doctor")) {
                return getDoctorMenu(model);//"doctor";
            }
            if (role.equals("nurse")) {
                return getNurseMenu(model);//"nurse";
            }
            if (role.equals("registry")) {
                return getRegistryMenu(model);//"registry";
            }
        }
        model.addAttribute("auth_status", auth_status);
        return "index";//"WEB-INF/jsp/index.jsp";
    }

    @RequestMapping("/")
    public String home() {
        return "redirect:index.html";//"WEB-INF/jsp/index.jsp";//"redirect:/index";
    }



    /*@RequestMapping(value = "/signin.html", method = RequestMethod.POST)
    public String signIn(@ModelAttribute("user") UserDTO user,
                         BindingResult result, Model model) {

        Role role = roleService.findById(user.getRole());
        User auth_user = userService.findUser(user.getName(), user.getPassword(), role);


        if (auth_user == null) {
            model.addAttribute("auth_status", "Wrong login or password");
            return "redirect:index.html";
        }
        if (auth_user.getRole().getRole().equals("patient")) {
            model.addAttribute("user_id", auth_user.getId());
            return "redirect:receptions.html";
        }
        if (auth_user.getRole().getRole().equals("doctor"))
            return "redirect:doctor.html";
        if (auth_user.getRole().getRole().equals("registry"))
            return "redirect:registry.html";
        if (auth_user.getRole().getRole().equals("nurse"))
            return "redirect:nurse.html";
        else
            return "redirect:index.html";
    }*/

    @RequestMapping(value = "/patient/analyses.html")
    public String getAnalyses(Model model) {
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Referral> referrals = referralService.getReferralsByUser(userService.findUserByName(user.getUsername()));
        model.addAttribute("analyses", referrals);
        return "patient_analyses";
    }

    @RequestMapping(value = "/patient/procedures.html")
    public String getProcedures(Model model) {
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Prescription> prescriptions = prescriptionService.getPrescriptionsByUser(userService.findUserByName(user.getUsername()));
        model.addAttribute("procedures", prescriptions);
        return "patient_procedures";
    }

    @RequestMapping(value = "/patient/receptions.html")
     public String getReceptions(Model model) {
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Appointment> appointments = appointmentService.getAppointmentsByPatient(userService.findUserByName(user.getUsername()));
        model.addAttribute("appointments", appointments);
        //appointments.get(0).getScheduleTime().getAppointmentTime().getStart()
        return "patient_receptions";
    }

    @RequestMapping(value = "/patient/receptions/new.html", method = RequestMethod.GET)
    public String setNewAppointment(Map<String, Object> map, Model model,
                                    @RequestParam(required=false) String incorrect) {

        User user = userService.findUserByName(
                ((org.springframework.security.core.userdetails.User)SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getPrincipal())
                .getUsername());
        Map<Integer, String> usersMap = new LinkedHashMap<Integer, String>();
        usersMap.put(user.getId(), user.getName());
        model.addAttribute("patientsList", usersMap);
        Map<Integer, String> doctorsMap = new LinkedHashMap<Integer, String>();
        List<User> users = userService.findUsersByRole(roleService.findByName("doctor"));
        doctorsMap.put(-1, "select doctor");
        for (int i = 0; i < users.size(); i++) {
            doctorsMap.put(users.get(i).getId(), users.get(i).getName());
        }

        model.addAttribute("doctorsList", doctorsMap);

        Map<Integer, String> dateMap = new LinkedHashMap<Integer, String>();
        dateMap.put(-1, "select day");
        dateMap.put(2, "monday");
        dateMap.put(3, "tuesday");
        dateMap.put(4, "wednesday");
        dateMap.put(5, "thursday");
        dateMap.put(6, "friday");
        dateMap.put(7, "saturday");
        model.addAttribute("daysList", dateMap);
        model.addAttribute("incorrect", incorrect);
        map.put("app", new NewAppointmentUserDTO());
        return "reception_booking_patient";
    }

    @RequestMapping(value = "/patient/receptions/add.html")
    public String addReception(@ModelAttribute("user") NewAppointmentUserDTO app) {

        if ((app.getStart() == null) || (app.getPatient() == null)) {
            return "redirect:/patient/receptions/new.html?incorrect=Appointment information is not correct";
        }
        Appointment appointment = new Appointment();

        DateMidnight dateMidnight = new DateMidnight();
        System.out.println("DAY OF WEEK: " + dateMidnight.dayOfWeek().get() + " " + app.getDay());
        if ((app.getDay() - 1) < dateMidnight.dayOfWeek().get()) {
            dateMidnight = dateMidnight.plusDays(app.getDay() - dateMidnight.dayOfWeek().get() - 1 + 7);
        }   else {
            dateMidnight = dateMidnight.plusDays(app.getDay() - dateMidnight.dayOfWeek().get() - 1);
        }
        System.out.println("DATE AFTER PLUS: " + dateMidnight.toString());
        java.sql.Date date = new java.sql.Date(dateMidnight.getMillis());
        appointment.setDate(date);

        appointment.setDiagnosis("");
        appointment.setDoctor(userService.findUserById(app.getDoctor()));
        appointment.setPatient(userService.findUserById(app.getPatient()));
        ScheduleTime scheduleTime = scheduleTimeService.findScheduleTimeById(app.getStart());
        if (!scheduleTime.getFree()) {
            //model.addAttribute("isFree", "this time is busy")
            if (!scheduleTime.getAppointment().getDate().before(new Date()))
                return "reception_booking_patient?is_free=this time is busy";
        }
        scheduleTime.setFree(false);
        scheduleTime.setAppointment(appointment);
        appointment.setScheduleTime(scheduleTime);
        appointmentService.addAppointment(appointment);
        scheduleTimeService.updateScheduleTime(scheduleTime);
        return "redirect:/patient/receptions.html";
    }

    @RequestMapping("/doctor.html")
    public String getDoctorMenu(Model model) {
        User user = userService.findUserByName(
                ((org.springframework.security.core.userdetails.User)SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getPrincipal())
                        .getUsername());

        DateMidnight dateMidnight = new DateMidnight();
        java.sql.Date date = new java.sql.Date(dateMidnight.getMillis());
        System.out.println("GET TIME: " + date.getTime());
        List<Appointment> appointments = appointmentService.getAppointmentsByDoctorAndDay(user, date);

        //Map<String, String> receptions = new HashMap<String, String>();
        List<NewAppointmentDTO> appointmentDTOList = new ArrayList<NewAppointmentDTO>();
        for (Appointment app : appointments) {
            //receptions.put(app.getScheduleTime().getAppointmentTime().getStart(), app.getPatient().getName());
            NewAppointmentDTO appointmentDTO = new NewAppointmentDTO();
            appointmentDTO.setId(app.getId());
            appointmentDTO.setPatient(app.getPatient().getName());
            appointmentDTO.setTime(app.getScheduleTime().getAppointmentTime().getStart());
            appointmentDTOList.add(appointmentDTO);
        }

        model.addAttribute("receptions", appointmentDTOList);
        return "doctor";
    }

    @RequestMapping(value = "/doctor/reception.html", method = RequestMethod.GET)
    public String getDoctorReceptionMenu(Model model, @RequestParam(required=false) String id) {
        User user = userService.findUserByName(
                ((org.springframework.security.core.userdetails.User)SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getPrincipal())
                        .getUsername());
        Integer id_app;

        id_app = Integer.parseInt(id);

        Appointment appointment = appointmentService.getAppointmentById(id_app);
        model.addAttribute("patient", appointment.getPatient().getName());

        List<ProcDTO> tests = new ArrayList<ProcDTO>();
        List<Referral> referrals = referralService.getReferralsByAppointment(appointment);

        for (Referral referral : referrals) {
            ProcDTO procDTO = new ProcDTO();
            procDTO.setNurse(referral.getNurse().getName());
            procDTO.setType(referral.getTest().getTest());
            procDTO.setId(referral.getId());
            tests.add(procDTO);
        }

        model.addAttribute("tests", tests);

        List<ProcDTO> treatments = new ArrayList<ProcDTO>();
        List<Prescription> prescriptions = prescriptionService.getPrescriptionsByAppointment(appointment);

        for (Prescription prescription : prescriptions) {
            ProcDTO procDTO = new ProcDTO();
            procDTO.setNurse(prescription.getNurse().getName());
            procDTO.setType(prescription.getTreatment().getTreatment());
            procDTO.setCount(prescription.getCount());
            procDTO.setId(prescription.getId());
            treatments.add(procDTO);
        }

        model.addAttribute("treatments", treatments);

        model.addAttribute("all_tests", testService.listTest());
        model.addAttribute("all_treatments", treatmentService.listTreatment());

        List<User> nurses = userService.findNursesByDoctor(user);

        model.addAttribute("nurses", nurses);

        model.addAttribute("id_app", id);

        return "reception";
    }

    @RequestMapping(value = "/registry.html")
    public String getRegistryMenu(Model model) {

        Map<Integer, String> specMap = new LinkedHashMap<Integer, String>();
        List<Specialization> specializations= specializationService.listSpecialization();
        specMap.put(-1, "--select specialization--");
        for (int i = 0; i < specializations.size(); i++) {
            specMap.put(specializations.get(i).getId(), specializations.get(i).getSpec_name());
        }
        model.addAttribute("specList", specMap);

        return "registry";
    }

    @RequestMapping(value = "/registry/appointments/new.html", method = RequestMethod.GET)
    public String setNewAppointmentFromRegistry(Map<String, Object> map, Model model,
                                                @RequestParam(required=false) String incorrect) {

        Map<Integer, String> usersMap = new LinkedHashMap<Integer, String>();
        List<User> users = userService.findUsersByRole(roleService.findByName("patient"));
        for (User user : users)
        {
            usersMap.put(user.getId(), user.getName());
        }
        model.addAttribute("patientsList", usersMap);
        Map<Integer, String> doctorsMap = new LinkedHashMap<Integer, String>();
        List<User> doctors = userService.findUsersByRole(roleService.findByName("doctor"));
        doctorsMap.put(-1, "--select doctor--");
        for (int i = 0; i < doctors.size(); i++) {
            doctorsMap.put(doctors.get(i).getId(), doctors.get(i).getName());
        }

        model.addAttribute("doctorsList", doctorsMap);

        Map<Integer, String> dateMap = new LinkedHashMap<Integer, String>();
        dateMap.put(-1, "select day");
        dateMap.put(2, "monday");
        dateMap.put(3, "tuesday");
        dateMap.put(4, "wednesday");
        dateMap.put(5, "thursday");
        dateMap.put(6, "friday");
        dateMap.put(7, "saturday");
        model.addAttribute("daysList", dateMap);
        model.addAttribute("incorrect", incorrect);
        map.put("app", new NewAppointmentUserDTO());
        return "reception_booking_patient";
    }

    @RequestMapping(value = "/registry/appointments/add.html")
    public String addReceptionFromRegistry(@ModelAttribute("user") NewAppointmentUserDTO app) {

        if ((app.getStart() == null) || (app.getPatient() == null)) {
            return "redirect:/registry/appointments/new.html?incorrect=Appointment information is not correct";
        }
        Appointment appointment = new Appointment();

        DateMidnight dateMidnight = new DateMidnight();
        java.sql.Date date = new java.sql.Date(dateMidnight.getMillis());
        appointment.setDate(date);

        appointment.setDiagnosis("");
        appointment.setDoctor(userService.findUserById(app.getDoctor()));
        appointment.setPatient(userService.findUserById(app.getPatient()));
        ScheduleTime scheduleTime = scheduleTimeService.findScheduleTimeById(app.getStart());
        if (!scheduleTime.getFree()) {
            if (!scheduleTime.getAppointment().getDate().before(new Date())) {
                System.out.println("NOT GOOD");
                return "reception_booking_patient";
            }
        }
        scheduleTime.setFree(false);

        scheduleTimeService.updateScheduleTime(scheduleTime);
        appointment.setScheduleTime(scheduleTime);
        appointmentService.addAppointment(appointment);
        return "redirect:/registry.html";
    }

    @RequestMapping(value = "/registry/registration.html", method = RequestMethod.GET)
    public String registration(Map<String, Object> map, Model model, @RequestParam(required=false) String reg_status) {

        Map<Integer, String> rolesMap = new LinkedHashMap<Integer, String>();
        List<Role> roles = roleService.listRole();
        if (roles.size() > 0) {
            rolesMap.put(-1, "--select role--");
        }
        for (int i = 0; i < roles.size(); i++) {
            rolesMap.put(roles.get(i).getId_role(), roles.get(i).getRole());
        }
        model.addAttribute("rolesList", rolesMap);

        Map<Integer, String> specializationsMap = new LinkedHashMap<Integer, String>();
        List<Specialization> specializations = specializationService.listSpecialization();
        if (specializations.size() > 0) {
            specializationsMap.put(-1, "--select specialization--");
        }
        for (int i = 0; i < specializations.size(); i++) {
            specializationsMap.put(specializations.get(i).getId(), specializations.get(i).getSpec_name());
        }
        model.addAttribute("specializationsList", specializationsMap);

        Map<Integer, String> doctorsMap = new LinkedHashMap<Integer, String>();
        List<User> doctors = userService.findUsersByRole(roleService.findByName("doctor"));
        if (doctors.size() > 0) {
            doctorsMap.put(-1, "--select doctor--");
        }
        for (int i = 0; i < doctors.size(); i++) {
            doctorsMap.put(doctors.get(i).getId(), doctors.get(i).getName());
        }
        model.addAttribute("doctorsList", doctorsMap);

        map.put("user", new UserDTO());
        model.addAttribute("reg_status", reg_status);
        return "registration";
    }

    @RequestMapping(value = "/registry/registration/signup.html", method = RequestMethod.POST)
    public String signUp(@ModelAttribute("user") UserDTO user,
                         Model model) {

        User u = userService.findUserByName(user.getName());
        if (u != null) {
            model.addAttribute("reg_status", "Username already exists");
            return "redirect:/registry/registration.html";
        }
        Specialization specialization = null;
        //Cabinet cabinet = null;
        User doctor = null;
        Role role = roleService.findById(user.getRole());
        if (role.getRole().equals("doctor")) {
            specialization = specializationService.findSpecializationById(user.getSpecialization());
            //cabinet        = cabinetService.findCabinetById(user.getCabinet());
            System.out.println("ADD USER: " + user.getName() + " " + user.getPassword() + " " + user.getRole() + " " + user.getCabinet() + " " + user.getSpecialization());
        }
        if (role.getRole().equals("nurse")) {
            doctor = userService.findUserById(user.getDoctor());
        }

        System.out.println("ADD USER: " + user.getName() + " " + user.getPassword() + " " + user.getRole());
        User newUser = new User();
        newUser.setDoctor(doctor);
        newUser.setName(user.getName());
        newUser.setPassword(user.getPassword());
        newUser.setRole(role);
        newUser.setSpecialization(specialization);
        userService.addUser(newUser);
        return "redirect:/registry.html";
    }

    @RequestMapping("/nurse.html")
    public String getNurseMenu(Model model) {
        User user = userService.findUserByName(
                ((org.springframework.security.core.userdetails.User) SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getPrincipal())
                        .getUsername());

        List<ProcDTO> tests = new ArrayList<ProcDTO>();
        List<Referral> referrals = referralService.getReferralsByNurse(user);

        for (Referral referral : referrals) {
            ProcDTO procDTO = new ProcDTO();
            procDTO.setNurse(referral.getNurse().getName());
            procDTO.setPatient(referral.getPatient().getName());
            procDTO.setType(referral.getTest().getTest());
            procDTO.setId(referral.getId());
            procDTO.setResult(referral.getResult());
            tests.add(procDTO);
        }

        model.addAttribute("tests", tests);

        List<ProcDTO> treatments = new ArrayList<ProcDTO>();
        List<Prescription> prescriptions = prescriptionService.getPrescriptionsByNurse(user);

        for (Prescription prescription : prescriptions) {
            ProcDTO procDTO = new ProcDTO();
            procDTO.setNurse(prescription.getNurse().getName());
            procDTO.setPatient(prescription.getPatient().getName());
            procDTO.setType(prescription.getTreatment().getTreatment());
            procDTO.setCount(prescription.getCount());
            procDTO.setId(prescription.getId());
            treatments.add(procDTO);
        }

        model.addAttribute("treatments", treatments);

        //model.addAttribute("name", name);
        return "nurse";
    }

    @RequestMapping("/access_denied.html")
     public String getAccessDeniedPage() {
        return "access_denied";
    }

    //////////////////////////////////////////////////////////////////////////////////////////ajax methods
    @RequestMapping(value="/patient/receptions/get_time.html", method=RequestMethod.POST)
    public @ResponseBody String getDataList(@RequestBody String param) {

        Integer day = -1;
        Integer doc = -1;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);

            JSONObject jsonObject = (JSONObject) obj;

            String dayStr = (String) jsonObject.get("day");
            String docStr = (String) jsonObject.get("doc");
            day = Integer.parseInt(dayStr);
            doc = Integer.parseInt(docStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        List<ScheduleTime> scheduleTimes =
                scheduleTimeService.findFreeScheduleTimeBySchedule(scheduleService.findScheduleByDayAndDoctor(day, userService.findUserById(doc)));

        Map<Integer, String> map = new HashMap<Integer, String>();
        for (ScheduleTime scheduleTime : scheduleTimes) {
            map.put(scheduleTime.getId(), scheduleTime.getAppointmentTime().getStart());
        }

        JSONArray jsonArray = new JSONArray();

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("key", entry.getKey());
            obj.put("value", entry.getValue());
            jsonArray.add(obj);
        }
        //System.out.println("AFTER JSON: " + jsonArray.toJSONString());
        return jsonArray.toJSONString();
    }

    @RequestMapping(value="/registry/get_doctors_by_specialization.html", method=RequestMethod.POST)
    public @ResponseBody String getDoctorsBySpecialization(@RequestBody String param) {

        Integer spec = -1;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);

            JSONObject jsonObject = (JSONObject) obj;

            String specStr = (String) jsonObject.get("spec");
            spec = Integer.parseInt(specStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        //System.out.println("GET DOCTORS BY SPECIALIZATION: SPEC: " + spec);
        List<User> users =
                userService.findUsersBySpecialization(specializationService.findSpecializationById(spec));
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (User user : users) {
            map.put(user.getId(), user.getName());
        }

        //System.out.println("GET DOCTORS BY SPECIALIZATION: USERS.SIZE: " + users.size());
        JSONArray jsonArray = new JSONArray();

        if (map.size() > 0) {
            JSONObject obj = new JSONObject();
            obj.put("key", "key");
            obj.put("value", "--select doctor--");
            jsonArray.add(obj);
        }

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("key", entry.getKey());
            obj.put("value", entry.getValue());
            jsonArray.add(obj);
        }
        //System.out.println("AFTER JSON: " + jsonArray.toJSONString());
        return jsonArray.toJSONString();
    }

    @RequestMapping(value="/registry/get_doctor_schedule_by_id.html", method=RequestMethod.POST)
    public @ResponseBody String getDoctorScheduleById(@RequestBody String param) {

        Integer doc = -1;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);

            JSONObject jsonObject = (JSONObject) obj;

            String docStr = (String) jsonObject.get("doc");
            doc = Integer.parseInt(docStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        Map<Integer, String> map = new HashMap<Integer, String>();

        //List<User> users =
        //        userService.findUsersBySpecialization(specializationService.findSpecializationById(spec));
        for (Integer i = 2; i <= 7; i++) {
            Schedule schedule = scheduleService.findScheduleByDayAndDoctor(i, userService.findUserById(doc));

            List<ScheduleTime> scheduleTimes =
                    scheduleTimeService.findScheduleTimeBySchedule(schedule);
            //System.out.println("SCHEDULETIMES: " + scheduleTimes.size());

            String start, finish;
            if (scheduleTimes.size() > 0) {
                start = scheduleTimes.get(0).getAppointmentTime().getStart();
                finish = scheduleTimes.get(0).getAppointmentTime().getFinish();
                for (ScheduleTime scheduleTime : scheduleTimes) {
                    if (scheduleTime.getAppointmentTime().getStart().compareTo(start) < 0) {
                        start = scheduleTime.getAppointmentTime().getStart();
                    }
                    if (scheduleTime.getAppointmentTime().getFinish().compareTo(finish) > 0) {
                        finish = scheduleTime.getAppointmentTime().getFinish();
                    }

                }
                map.put(schedule.getDay(), start.concat(" - ").concat(finish));
            }
        }

        JSONArray jsonArray = new JSONArray();

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("key", getDay(entry.getKey()));
            obj.put("value", entry.getValue());
            jsonArray.add(obj);
        }
        //System.out.println("AFTER JSON: " + jsonArray.toJSONString());
        return jsonArray.toJSONString();
    }

    @RequestMapping(value="/registry/get_cabinets_by_spec.html", method=RequestMethod.POST)
    public @ResponseBody String getCabinetsBySpec(@RequestBody String param) {

        //System.out.println("HERE WE ARE: PARAM: " + param);
        Integer spec = -1;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);

            JSONObject jsonObject = (JSONObject) obj;

            String specStr = (String) jsonObject.get("spec");
            spec = Integer.parseInt(specStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        //System.out.println("HERE WE ARE: SPEC: " + spec);
        Map<Integer, String> map = new HashMap<Integer, String>();

        List<Cabinet> cabinets =
                cabinetService.findCabinetsBySpecialization(specializationService.findSpecializationById(spec));
        for (Cabinet cabinet : cabinets) {
            map.put(cabinet.getId(), cabinet.getNumber());
        }

        JSONArray jsonArray = new JSONArray();

        if (cabinets.size() > 0) {
            JSONObject obj = new JSONObject();
            obj.put("key", -1);
            obj.put("value", "--select cabinet--");
            jsonArray.add(obj);
        }

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("key", entry.getKey());
            obj.put("value", entry.getValue());
            jsonArray.add(obj);
        }

        //System.out.println("HERE WE ARE: JSON: " + jsonArray.toJSONString());
        return jsonArray.toJSONString();
    }

    @RequestMapping(value="/doctor/save_app.html", method=RequestMethod.POST)
    public @ResponseBody String saveAppointment(@RequestBody String param) {

        String diagnosis;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);
            JSONObject jsonObject = (JSONObject) obj;
            diagnosis = (String) jsonObject.get("diagnosis");
            Integer app_id = Integer.parseInt((String)jsonObject.get("app"));

            String patient_name = (String)jsonObject.get("patient");
            System.out.println("SAVE APP: " + diagnosis + " " + app_id + " " + patient_name);
            Appointment appointment = appointmentService.getAppointmentById(app_id);

            appointment.setDiagnosis(diagnosis);
            appointmentService.updateAppointment(appointment);


            User patient = userService.findUserByName(patient_name);

            JSONArray jsonArrayTest = (JSONArray) jsonObject.get("tests");
            Iterator i = jsonArrayTest.iterator();
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
                Integer test_id = Integer.parseInt((String) innerObj.get("id"));
                String type = (String)innerObj.get("type");
                String nurse_name = (String)innerObj.get("nurse");
                User nurse = userService.findUserByName(nurse_name);
                Referral referral = new Referral();
                referral.setAppointment(appointment);
                referral.setNurse(nurse);
                referral.setPatient(patient);
                Test test = testService.findTestByName(type);
                referral.setTest(test);
                referral.setResult("");
                System.out.println("SAVE APP: " + test_id + " " + nurse_name + " " + test.getTest());
                //referrals.add(referral);
                referralService.addReferral(referral);
            }

            jsonArrayTest = (JSONArray) jsonObject.get("treatments");
            i = jsonArrayTest.iterator();
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
                Integer treatment_id = Integer.parseInt((String)innerObj.get("id"));
                Integer count = Integer.parseInt((String)innerObj.get("count"));
                String type = (String)innerObj.get("type");
                String nurse_name = (String)innerObj.get("nurse");
                User nurse = userService.findUserByName(nurse_name);
                Prescription prescription = new Prescription();
                prescription.setAppointment(appointment);
                prescription.setNurse(nurse);
                prescription.setPatient(patient);
                prescription.setCount(count);
                prescription.setTreatment(treatmentService.findTreatmentByName(type));
                prescriptionService.addPrescription(prescription);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return "ok";
    }

    @RequestMapping(value="/nurse/save_proc_nurse.html", method=RequestMethod.POST)
    public @ResponseBody String saveProc(@RequestBody String param) {

        //String diagnosis;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(param);
            JSONObject jsonObject = (JSONObject) obj;

            JSONArray jsonArrayTest = (JSONArray) jsonObject.get("tests");
            Iterator i = jsonArrayTest.iterator();
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
                String id = ((String) innerObj.get("id"));
                //String patient_name = (String)innerObj.get("patient");
                //User patient = userService.findUserByName(patient_name);
                //String type = (String)innerObj.get("type");
                String result = (String)innerObj.get("result");
                Pattern pat=Pattern.compile("[-]?[0-9]+(.[0-9]+)?");
                Matcher matcher=pat.matcher(id);
                Integer referral_id = -1;
                while (matcher.find()) {
                    referral_id = Integer.parseInt(matcher.group());
                    System.out.println("REFERRAL ID: " + referral_id);
                };
                Referral referral = referralService.getReferralById(referral_id);
                referral.setResult(result);
                /*referral.setPatient(patient);
                Test test = testService.findTestByName(type);
                referral.setTest(test);
                referral.setResult(""); */
                //System.out.println("SAVE APP: " + test_id + " " + nurse_name + " " + test.getTest());
                //referrals.add(referral);
                referralService.updateReferral(referral);
            }

            jsonArrayTest = (JSONArray) jsonObject.get("treatments");
            i = jsonArrayTest.iterator();
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
                String id = ((String) innerObj.get("id"));
                //String patient_name = (String)innerObj.get("patient");
                //User patient = userService.findUserByName(patient_name);
                //String type = (String)innerObj.get("type");
                Integer count = (Integer)(int)(long)(Long)innerObj.get("count");
                Pattern pat=Pattern.compile("[-]?[0-9]+(.[0-9]+)?");
                Matcher matcher=pat.matcher(id);
                Integer prescription_id = -1;
                while (matcher.find()) {
                    prescription_id = Integer.parseInt(matcher.group());
                    System.out.println("PRESCRIPTION ID: " + prescription_id);
                };
                Prescription prescription = prescriptionService.getPrescriptionById(prescription_id);
                prescription.setCount(count);
                /*referral.setPatient(patient);
                Test test = testService.findTestByName(type);
                referral.setTest(test);
                referral.setResult(""); */
                //System.out.println("SAVE APP: " + test_id + " " + nurse_name + " " + test.getTest());
                //referrals.add(referral);
                prescriptionService.updatePrescription(prescription);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return "ok";
    }

    private String getDay(Integer num) {
        switch (num) {
            case 2: return "monday";
            case 3: return "tuesday";
            case 4: return "wednesday";
            case 5: return "thursday";
            case 6: return "friday";
            case 7: return "saturday";
        }
        return "";
    }
}
