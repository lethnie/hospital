package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.TestDAO;
import ru.vsu.cs.hospital.dao.TestDAOImpl;
import ru.vsu.cs.hospital.domain.Test;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
@Service
public class TestServiceImpl implements TestService{
    @Autowired
    private TestDAO testDAO;

    @Transactional
    public void addTest(Test test) {
        testDAO.addTest(test);
    }

    @Transactional
    public List<Test> listTest() {

        return testDAO.listTest();
    }

    @Transactional
    public Test findTestById(Integer id) {
        return testDAO.findTestById(id);
    }

    @Transactional
    public Test findTestByName(String name) {
        return testDAO.findTestByName(name);
    }

    @Transactional
    public void removeContact(Integer id) {
        testDAO.removeTest(id);
    }
}
