package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import ru.vsu.cs.hospital.domain.*;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class AppointmentDAOImpl implements AppointmentDAO {

    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addAppointment(Appointment appointment) {
        sessionFactory.getCurrentSession().save(appointment);
    }

    @SuppressWarnings("unchecked")
    public List<Appointment> listAppointment() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Appointment")
                .list();
    }

    public void removeAppointment(Integer id) {
        Appointment appointment = (Appointment) sessionFactory.getCurrentSession().load(
                Appointment.class, id);
        if (null != appointment) {
            sessionFactory.getCurrentSession().delete(appointment);
        }

    }

    public List<Appointment> getAppointmentsByPatient(User user) {
        List<Appointment> appointments;
        appointments = sessionFactory.getCurrentSession().createCriteria(Appointment.class)
                //.createAlias("appointmentTime", "app")
                .addOrder(Order.asc("date"))
                .add(Restrictions.eq("patient", user))
                .list();
        for (Appointment appointment : appointments) {
            Hibernate.initialize(appointment.getDoctor());
            Hibernate.initialize(appointment.getPatient());
            Hibernate.initialize(appointment.getScheduleTime());
            //TODO: ???
            Hibernate.initialize(appointment.getScheduleTime().getAppointmentTime());
        }
        return appointments;
    }

    public List<Appointment> getAppointmentsByDoctor(User user) {
        List<Appointment> appointments;
        appointments = sessionFactory.getCurrentSession().createCriteria(Appointment.class)
                .add(Restrictions.eq("doctor", user)).list();
        for (Appointment appointment : appointments) {
            Hibernate.initialize(appointment.getDoctor());
            Hibernate.initialize(appointment.getPatient());
            Hibernate.initialize(appointment.getScheduleTime());
            Hibernate.initialize(appointment.getScheduleTime().getAppointmentTime());
        }
        return appointments;
    }

    public List<Appointment> getAppointmentsByDoctorAndDay(User user, Date date) {
        List<Appointment> appointments;
        appointments = sessionFactory.getCurrentSession().createCriteria(Appointment.class)
                .add(Restrictions.eq("doctor", user))
                .add(Restrictions.eq("date", date))
                .list();
        for (Appointment appointment : appointments) {
            Hibernate.initialize(appointment.getDoctor());
            Hibernate.initialize(appointment.getPatient());
            Hibernate.initialize(appointment.getScheduleTime());
            Hibernate.initialize(appointment.getScheduleTime().getAppointmentTime());
        }
        /*appointments = getAppointmentsByDoctor(user);
        for (Appointment app : appointments) {
            System.out.println("APP DAO: app.date.time: " + app.getDate().getTime());
        }*/
        return appointments;
    }

    public Appointment getAppointmentById(Integer id) {
        Appointment result = (Appointment)sessionFactory.getCurrentSession().createCriteria(Appointment.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        Hibernate.initialize(result.getScheduleTime());
        Hibernate.initialize(result.getPatient());
        Hibernate.initialize(result.getDoctor());
        sessionFactory.getCurrentSession().update(result);
        return result;
    }

    public void updateAppointment(Appointment appointment) {
        sessionFactory.getCurrentSession().update(appointment);
    }
}
