package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Schedule;
import ru.vsu.cs.hospital.domain.ScheduleTime;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public interface ScheduleTimeDAO {
    public void addScheduleTime(ScheduleTime scheduleTime);
    public void updateScheduleTime(ScheduleTime scheduleTime);
    public List<ScheduleTime> listScheduleTime();
    public ScheduleTime findScheduleTimeById(Integer id);
    public void removeScheduleTime(Integer id);
    public List<ScheduleTime> findFreeScheduleTimeBySchedule(Schedule schedule);
    public List<ScheduleTime> findScheduleTimeBySchedule(Schedule schedule);
    //public void setScheduleTimeFree(ScheduleTime st, Boolean isFree);
}
