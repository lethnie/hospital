package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import ru.vsu.cs.hospital.dao.UserDAO;
import ru.vsu.cs.hospital.dao.UserDAOImpl;
import ru.vsu.cs.hospital.domain.Role;
import ru.vsu.cs.hospital.domain.Specialization;
import ru.vsu.cs.hospital.domain.User;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:19
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Transactional
    public void addUser(User user) {
        userDAO.addUser(user);
    }

    @Transactional
    public List<User> listUser() {

        return userDAO.listUser();
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public User findUser(String name, String password, Role role) {
        //System.out.println(name + " " +  password);
        return userDAO.findUser(name, password, role);
    }

    @Transactional
    public User findUserById(Integer id) {
        return userDAO.findUserById(id);
    }

    @Transactional
    public User findUserByName(String name) {
        return userDAO.findUserByName(name);
    }

    @Transactional
    public List<User> findUsersByRole(Role role) {
        return userDAO.findUsersByRole(role);
    }

    @Transactional
    public List<User> findNursesByDoctor(User doctor) {
        return userDAO.findNursesByDoctor(doctor);
    }

    @Transactional
    public List<User> findUsersBySpecialization(Specialization specialization) {
        return userDAO.findUsersBySpecialization(specialization);
    }

    @Transactional
    public void removeContact(Integer id) {
        userDAO.removeUser(id);
    }
}
