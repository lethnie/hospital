package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 15:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="schedule_time")
public class ScheduleTime implements Serializable {
    public static final String id_column = "sch_time_id";
    //public static final String sch_column = "schedule_id";
    //public static final String app_time_column = "app_time_id";
    public static final String free_column = "free";
    //public static final String app_column = "appointment_id";

    public ScheduleTime() {
    }

    private Integer id;
    private Schedule schedule;
    private AppointmentTime appointmentTime;
    private Boolean free;
    private Appointment appointment;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="schedule_time_sequence")
    @GenericGenerator(name="schedule_time_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //@Column(name=sch_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Schedule.id_column, nullable = false)
    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    //@Column(name=app_time_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=AppointmentTime.id_column, nullable = false)
    public AppointmentTime getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(AppointmentTime appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    @Column(name=free_column, nullable = false)
    public Boolean getFree() {
        return free;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }

    //@Column(name=app_column)
    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Appointment.id_column)
    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
}
