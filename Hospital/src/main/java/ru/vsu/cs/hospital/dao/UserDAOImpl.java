package ru.vsu.cs.hospital.dao;

import org.hibernate.*;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUser() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.User")
                .list();
    }

    @SuppressWarnings("unchecked")
    public User findUser(String name, String password, Role role) {
        /*List<User> result = sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.User where ? = ? and ? = ?")
                .setParameter(0, User.name_column)
                .setParameter(1, name)
                .setParameter(2, User.password_column)
                .setParameter(3, password)
                .list();*/
        //List<User> result = sessionFactory.getCurrentSession()//.createSQLQuery("select * from user_table where name = ? and password = ?").setParameter(0, name).setParameter(1, password).list();
        //                                    .createQuery("from ru.vsu.cs.hospital.domain.User user where user.name = 'user'")

        List<User> result = sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Expression.like(User.name_column, name))
                .add(Expression.like(User.password_column, password))
                .add(Restrictions.eq("role", role))
                        .list();
        System.out.println(result.size());
        //System.out.println(result.get(0).getName());
        for (User user : result) {
            Hibernate.initialize(user.getRole());
            Hibernate.initialize(user.getSpecialization());
            Hibernate.initialize(user.getDoctor());
            sessionFactory.getCurrentSession().update(user);
        }
        if (result.size() == 0)
            return null;
        else {
            //sessionFactory.getCurrentSession().update(result.get(0));
            return result.get(0);
        }
    }

    public User findUserById(Integer id) {
        User result = (User)sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        if (result != null) {
            Hibernate.initialize(result.getRole());
            Hibernate.initialize(result.getSpecialization());
            Hibernate.initialize(result.getDoctor());
            sessionFactory.getCurrentSession().update(result);
        }
            return result;
    }

    public User findUserByName(String name) {
        User result = (User)sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq(User.name_column, name))
                .uniqueResult();
        if (result != null) {
            Hibernate.initialize(result.getRole());
            Hibernate.initialize(result.getSpecialization());
            Hibernate.initialize(result.getDoctor());
            sessionFactory.getCurrentSession().update(result);
        }
        return result;
    }

    public List<User> findUsersByRole(Role role) {
        List<User> result = sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("role", role))
                .list();
        for (User user : result) {
            Hibernate.initialize(user.getRole());
            Hibernate.initialize(user.getSpecialization());
            Hibernate.initialize(user.getDoctor());
            sessionFactory.getCurrentSession().update(user);
        }

        return result;
    }

    public List<User> findNursesByDoctor(User doctor) {
        List<User> result = sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("doctor", doctor))
                .list();
        for (User user : result) {
            Hibernate.initialize(user.getRole());
            Hibernate.initialize(user.getSpecialization());
            Hibernate.initialize(user.getDoctor());
            sessionFactory.getCurrentSession().update(user);
        }

        return result;
    }

    public List<User> findUsersBySpecialization(Specialization specialization) {
        List<User> result = sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("specialization", specialization))
                .list();
        if (result == null)
            return new ArrayList<User>();
        for (User user : result) {
            Hibernate.initialize(user.getRole());
            Hibernate.initialize(user.getSpecialization());
            Hibernate.initialize(user.getDoctor());
            sessionFactory.getCurrentSession().update(user);
        }

        return result;
    }

    public void removeUser(Integer id) {
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
        }

    }
}
