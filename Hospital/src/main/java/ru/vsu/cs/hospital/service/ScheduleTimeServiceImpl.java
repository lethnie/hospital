package ru.vsu.cs.hospital.service;

import org.joda.time.DateMidnight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.ScheduleTimeDAO;
import ru.vsu.cs.hospital.domain.Schedule;
import ru.vsu.cs.hospital.domain.ScheduleTime;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ScheduleTimeServiceImpl implements ScheduleTimeService {
    @Autowired
    private ScheduleTimeDAO scheduleTimeDAO;

    @Transactional
    public void addScheduleTime(ScheduleTime referral) {
        scheduleTimeDAO.addScheduleTime(referral);
    }

    @Transactional
    public List<ScheduleTime> listScheduleTime() {

        return scheduleTimeDAO.listScheduleTime();
    }

    @Transactional
    public ScheduleTime findScheduleTimeById(Integer id) {
        return scheduleTimeDAO.findScheduleTimeById(id);
    }

    @Transactional
    public void removeContact(Integer id) {
        scheduleTimeDAO.removeScheduleTime(id);
    }

    @Transactional
    public void updateScheduleTime(ScheduleTime st) {
        scheduleTimeDAO.updateScheduleTime(st);
    }

    @Transactional
     public List<ScheduleTime> findFreeScheduleTimeBySchedule(Schedule schedule) {
        List<ScheduleTime> scheduleTimes = scheduleTimeDAO.findScheduleTimeBySchedule(schedule);
        DateMidnight dateMidnight = new DateMidnight();
        java.sql.Date date = new java.sql.Date(dateMidnight.getMillis());
        for (int i = 0; i < scheduleTimes.size(); i++) {
            ScheduleTime scheduleTime = scheduleTimes.get(i);
            if (!scheduleTime.getFree()) {
                if (scheduleTime.getAppointment() == null) {
                    scheduleTime.setFree(true);
                    scheduleTime.setAppointment(null);
                    updateScheduleTime(scheduleTime);
                } else {
                    if (scheduleTime.getAppointment().getDate().before(date)) {
                        scheduleTime.setFree(true);
                        scheduleTime.setAppointment(null);
                        scheduleTimeDAO.updateScheduleTime(scheduleTime);
                    }


                    else {
                        scheduleTimes.remove(scheduleTime);
                        i--;
                    }
                }
            }
        }
        //System.out.println("AFTER GET ARRAY");
        /*for (int i = 0; i < scheduleTimes.size() - 1; i++) {
            for (int j = i + 1; j < scheduleTimes.size(); j++) {
                if (scheduleTimes.get(i).getAppointmentTime().getStart().compareTo(scheduleTimes.get(j).getAppointmentTime().getStart()) > 0) {
                    ScheduleTime scheduleTime = scheduleTimes.get(i);
                    scheduleTimes.set(i, scheduleTimes.get(j));
                    scheduleTimes.set(j, scheduleTime);
                }
            }
        }*/
        return scheduleTimes;
    }

    @Transactional
    public List<ScheduleTime> findScheduleTimeBySchedule(Schedule schedule) {
        return scheduleTimeDAO.findScheduleTimeBySchedule(schedule);
    }
}
