package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Role;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:43
 * To change this template use File | Settings | File Templates.
 */
public interface RoleService {
    public void addRole(Role role);
    public List<Role> listRole();
    public void removeContact(Integer id);
    public Role findById(Integer id);
    public Role findByName(String name);
}
