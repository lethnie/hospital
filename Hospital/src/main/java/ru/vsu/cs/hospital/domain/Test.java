/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author 123
 */
@Entity
@Table(name="test")
public class Test implements Serializable {
    public static final String id_column = "test_id";
    public static final String test_column = "test_name";

    public Test() {        
    }
    
    private Integer id;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="test_sequence")
    @GenericGenerator(name="test_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer i) {
        id = i;
    }
    
    private String test;

    @Column(name=test_column, nullable = false)
    public String getTest() {
        return test;
    }
    
    public void setTest(String t) {
        test = t;
    }
}
