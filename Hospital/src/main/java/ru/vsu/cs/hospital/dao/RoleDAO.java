package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Role;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public interface RoleDAO {
    public void addRole(Role role);
    public List<Role> listRole();
    public void removeRole(Integer id);
    public Role findById(Integer id);
    public Role findByName(String name);
}
