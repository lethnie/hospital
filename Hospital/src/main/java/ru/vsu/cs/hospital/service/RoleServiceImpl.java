package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.RoleDAO;
import ru.vsu.cs.hospital.dao.RoleDAOImpl;
import ru.vsu.cs.hospital.domain.Role;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDAO roleDAO;

    @Transactional
    public void addRole(Role role) {
        roleDAO.addRole(role);
    }

    @Transactional
    public List<Role> listRole() {

        return roleDAO.listRole();
    }

    @Transactional
    public Role findById(Integer id) {
        return roleDAO.findById(id);
    }

    @Transactional
    public Role findByName(String name) {
         return roleDAO.findByName(name);
    }

    @Transactional
    public void removeContact(Integer id) {
        roleDAO.removeRole(id);
    }
}
