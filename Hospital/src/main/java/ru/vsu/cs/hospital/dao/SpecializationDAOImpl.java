package ru.vsu.cs.hospital.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 18:05
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class SpecializationDAOImpl implements SpecializationDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addSpecialization(Specialization specialization) {
        sessionFactory.getCurrentSession().save(specialization);
    }

    @SuppressWarnings("unchecked")
    public List<Specialization> listSpecialization() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Specialization")
                .list();
    }

    public Specialization findSpecializationById(Integer id) {
        Specialization specialization = (Specialization) sessionFactory.getCurrentSession().load(
                Specialization.class, id);
        return specialization;
    }

    public void removeSpecialization(Integer id) {
        Specialization specialization = (Specialization) sessionFactory.getCurrentSession().load(
                Specialization.class, id);
        if (null != specialization) {
            sessionFactory.getCurrentSession().delete(specialization);
        }

    }
}
