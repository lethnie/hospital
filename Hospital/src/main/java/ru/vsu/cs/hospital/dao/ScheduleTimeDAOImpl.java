package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ScheduleTimeDAOImpl implements ScheduleTimeDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addScheduleTime(ScheduleTime scheduleTime) {
        sessionFactory.getCurrentSession().save(scheduleTime);
    }

    @SuppressWarnings("unchecked")
    public List<ScheduleTime> listScheduleTime() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.ScheduleTime")
                .list();
    }

    public ScheduleTime findScheduleTimeById(Integer id) {
        ScheduleTime result = (ScheduleTime)sessionFactory.getCurrentSession().createCriteria(ScheduleTime.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        Hibernate.initialize(result.getAppointment());
        Hibernate.initialize(result.getAppointmentTime());
        Hibernate.initialize(result.getSchedule());
        sessionFactory.getCurrentSession().update(result);
        return result;
    }

    public List<ScheduleTime> findFreeScheduleTimeBySchedule(Schedule schedule) {
        if (schedule == null)
            return new ArrayList<ScheduleTime>();
        List<ScheduleTime> result = sessionFactory.getCurrentSession().createCriteria(ScheduleTime.class)
                .add(Restrictions.eq("schedule", schedule))
                .add(Restrictions.eq("free", true))
                .list();
        //java.util.Date date = new java.util.Date();
        for (ScheduleTime scheduleTime : result) {
            Hibernate.initialize(scheduleTime.getAppointment());
            Hibernate.initialize(scheduleTime.getAppointmentTime());
            Hibernate.initialize(scheduleTime.getSchedule());
            sessionFactory.getCurrentSession().update(scheduleTime);
            /*if (!scheduleTime.getFree()) {
                if (scheduleTime.getAppointment().getDate().before(new Date(date.getTime()))) {
                    scheduleTime.setFree(true);
                    updateScheduleTime(scheduleTime);
                }
            }*/
        }
        return result;
    }

    public List<ScheduleTime> findScheduleTimeBySchedule(Schedule schedule) {
        if (schedule == null)
            return new ArrayList<ScheduleTime>();
        List<ScheduleTime> result = sessionFactory.getCurrentSession().createCriteria(ScheduleTime.class)
                .add(Restrictions.eq("schedule", schedule))
                .createAlias("appointmentTime", "app")
                .addOrder(Order.asc("app.start"))
                .list();
        for (ScheduleTime scheduleTime : result) {
            Hibernate.initialize(scheduleTime.getAppointment());
            Hibernate.initialize(scheduleTime.getAppointmentTime());
            Hibernate.initialize(scheduleTime.getSchedule());
            sessionFactory.getCurrentSession().update(scheduleTime);
        }
        return result;
    }

    public void removeScheduleTime(Integer id) {
        ScheduleTime scheduleTime = (ScheduleTime) sessionFactory.getCurrentSession().load(
                ScheduleTime.class, id);
        if (null != scheduleTime) {
            sessionFactory.getCurrentSession().delete(scheduleTime);
        }

    }

    public void updateScheduleTime(ScheduleTime scheduleTime) {
        sessionFactory.getCurrentSession().update(scheduleTime);
    }
}
