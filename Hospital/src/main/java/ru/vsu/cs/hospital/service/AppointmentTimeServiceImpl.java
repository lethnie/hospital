package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.AppointmentTimeDAO;
import ru.vsu.cs.hospital.dao.AppointmentTimeDAOImpl;
import ru.vsu.cs.hospital.domain.AppointmentTime;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
@Service
public class AppointmentTimeServiceImpl implements AppointmentTimeService {
    @Autowired
    private AppointmentTimeDAO appointmentTimeDAO;

    @Transactional
    public void addAppointmentTime(AppointmentTime appointmentTime) {
        appointmentTimeDAO.addAppointmentTime(appointmentTime);
    }

    @Transactional
    public List<AppointmentTime> listAppointmentTime() {

        return appointmentTimeDAO.listAppointmentTime();
    }

    @Transactional
    public void removeContact(Integer id) {
        appointmentTimeDAO.removeAppointmentTime(id);
    }
}
