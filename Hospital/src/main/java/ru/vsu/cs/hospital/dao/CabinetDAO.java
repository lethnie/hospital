package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Cabinet;
import ru.vsu.cs.hospital.domain.Specialization;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public interface CabinetDAO {
    public void addCabinet(Cabinet cabinet);
    public List<Cabinet> listCabinet();
    public List<Cabinet> findCabinetsBySpecialization(Specialization specialization);
    public Cabinet findCabinetById(Integer id);
    public void removeCabinet(Integer id);
}