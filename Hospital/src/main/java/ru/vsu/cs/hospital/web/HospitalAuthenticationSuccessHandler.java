package ru.vsu.cs.hospital.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 10.05.14
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class HospitalAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        try {
            if (roles.contains("ROLE_PATIENT")) {
                response.sendRedirect("patient/receptions.html");
                return;
            }
            if (roles.contains("ROLE_DOCTOR")) {
                response.sendRedirect("doctor.html");
                    return;
            }
            if (roles.contains("ROLE_NURSE")) {
                response.sendRedirect("nurse.html");
                return;
            }
            if (roles.contains("ROLE_REGISTRY")) {
                response.sendRedirect("registry.html");
                return;
            }
        }
        catch (IOException ex) {
             System.out.println(ex.getMessage());
        }
    }
}
