package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Cabinet;
import ru.vsu.cs.hospital.domain.Specialization;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 19.04.14
 * Time: 0:31
 * To change this template use File | Settings | File Templates.
 */
public interface CabinetService {
    public void addCabinet(Cabinet cabinet);
    public List<Cabinet> listCabinet();
    public List<Cabinet> findCabinetsBySpecialization(Specialization specialization);
    public Cabinet findCabinetById(Integer id);
    public void removeContact(Integer id);
}
