package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public interface TestDAO {
    public void addTest(Test test);

    public List<Test> listTest();

    public Test findTestById(Integer id);

    public Test findTestByName(String name);

    public void removeTest(Integer id);
}
