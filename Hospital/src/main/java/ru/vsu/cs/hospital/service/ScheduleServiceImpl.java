package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.ScheduleDAO;
import ru.vsu.cs.hospital.dao.ScheduleDAOImpl;
import ru.vsu.cs.hospital.domain.Schedule;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private ScheduleDAO scheduleDAO;

    @Transactional
    public void addSchedule(Schedule schedule) {
        scheduleDAO.addSchedule(schedule);
    }

    @Transactional
    public List<Schedule> listSchedule() {

        return scheduleDAO.listSchedule();
    }

    @Transactional
    public Schedule findScheduleByDayAndDoctor(Integer day, User doctor) {
        return scheduleDAO.findScheduleByDayAndDoctor(day, doctor);
    }

    @Transactional
    public void removeContact(Integer id) {
        scheduleDAO.removeSchedule(id);
    }
}
