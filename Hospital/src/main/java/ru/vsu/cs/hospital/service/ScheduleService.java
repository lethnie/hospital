package ru.vsu.cs.hospital.service;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.Schedule;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:43
 * To change this template use File | Settings | File Templates.
 */
public interface ScheduleService {
    public void addSchedule(Schedule schedule);
    public List<Schedule> listSchedule();
    public Schedule findScheduleByDayAndDoctor(Integer day, User doctor);
    public void removeContact(Integer id);
}
