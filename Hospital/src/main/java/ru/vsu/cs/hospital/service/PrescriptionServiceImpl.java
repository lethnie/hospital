package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.PrescriptionDAO;
import ru.vsu.cs.hospital.dao.PrescriptionDAOImpl;
import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.Prescription;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:13
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PrescriptionServiceImpl implements PrescriptionService {
    @Autowired
    private PrescriptionDAO prescriptionDAO;

    @Transactional
    public void addPrescription(Prescription prescription) {
        prescriptionDAO.addPrescription(prescription);
    }

    @Transactional
    public List<Prescription> listPrescription() {

        return prescriptionDAO.listPrescription();
    }

    @Transactional
    public List<Prescription> getPrescriptionsByUser(User user) {
        return prescriptionDAO.getPrescriptionsByUser(user);
    }

    @Transactional
    public List<Prescription> getPrescriptionsByNurse(User user) {
        return prescriptionDAO.getPrescriptionsByNurse(user);
    }

    @Transactional
    public List<Prescription> getPrescriptionsByAppointment(Appointment appointment) {
        return prescriptionDAO.getPrescriptionsByAppointment(appointment);
    }

    @Transactional
    public Prescription getPrescriptionById(Integer id) {
         return prescriptionDAO.getPrescriptionById(id);
    }

    @Transactional
    public void updatePrescription(Prescription prescription) {
        prescriptionDAO.updatePrescription(prescription);
    }

    @Transactional
    public void removeContact(Integer id) {
        prescriptionDAO.removePrescription(id);
    }
}
