package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="role")
public class Role implements Serializable {
    public static final String id_column = "role_id";
    public static final String role_column = "role_name";

    public Role() {
    }

    private Integer id_role;

    private String role;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="role_sequence")
    @GenericGenerator(name="role_sequence", strategy = "increment")
    public Integer getId_role() {
        return id_role;
    }

    public void setId_role(Integer id_role) {
        this.id_role = id_role;
    }

    @Column(name=role_column, nullable = false)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
