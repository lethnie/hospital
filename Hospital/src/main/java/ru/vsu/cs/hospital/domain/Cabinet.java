package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="cabinet")
public class Cabinet implements Serializable {
    public static final String id_column = "cabinet_id";
    public static final String num_column = "number";
    //public static final String spec_column = "specialization_id";

    public Cabinet() {
    }

    private Integer id;
    private String number;
    private Specialization specialization;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="cabinet_sequence")
    @GenericGenerator(name="cabinet_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name=num_column, nullable = false)
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    //@Column(name=spec_column)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Specialization.id_column)
    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }
}
