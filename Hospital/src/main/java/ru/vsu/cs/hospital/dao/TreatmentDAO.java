package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Treatment;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
public interface TreatmentDAO {
    public void addTreatment(Treatment treatment);

    public List<Treatment> listTreatment();

    public Treatment findTreatmentById(Integer id);

    public Treatment findTreatmentByName(String name);

    public void removeTreatment(Integer id);
}
