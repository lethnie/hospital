package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="user_table")
public class User implements Serializable {
    public static final String id_column = "user_id";
    public static final String name_column = "name";
    public static final String password_column = "password";
    //public static final String role_column = "role_id";
    public static final String doctor_column = "doctor_id";
    //public static final String specialization_column = "specialization_id";

    public User() {
    }

    public User(Integer id, String name, String password, Role role, User doctor, Specialization specialization) {

        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.doctor = doctor;
        this.specialization = specialization;
    }

    private Integer id;
    private String name;
    private String password;
    private Role role;
    private User doctor;
    private Specialization specialization;


    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="user_sequence")
    @GenericGenerator(name="user_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name=name_column, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name=password_column, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //@ForeignKey
    //@Column(name=role_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Role.id_column, nullable = false)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    //@Column(name=doctor_column)
    @ManyToOne(fetch= FetchType.LAZY/*, cascade={CascadeType.ALL}*/)
    @JoinColumn(name=doctor_column)
    public User getDoctor() {
        return doctor;
    }

    /*@OneToMany(mappedBy="doctor", cascade=CascadeType.ALL)
    @ElementCollection(targetClass=User.class)
    private Set<User> nurses = new HashSet<User>();*/

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    //@Column(name=specialization_column)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Specialization.id_column)
    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    /*public Set<User> getNurses() {
        return nurses;
    }

    public void setNurses(Set<User> nurses) {
        this.nurses = nurses;
    }*/
}
