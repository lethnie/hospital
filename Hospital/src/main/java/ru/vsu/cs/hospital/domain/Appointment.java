/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author 123
 */
@Entity
@Table(name="appointment")
public class Appointment implements Serializable {
    public static final String diagnosis_column = "diagnosis";
    public static final String date_column = "date";
    public static final String id_column = "appointment_id";
    public static final String doctor_column = "doctor_id";
    public static final String patient_column = "patient_id";
    /*public static final String sch_time_column = "sch_time_id";*/

    public Appointment() {
    }

    private Integer id;
    private String diagnosis;
    private Date date;
    private User doctor;
    private User patient;
    private ScheduleTime scheduleTime;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="appointment_sequence")
    @GenericGenerator(name="appointment_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer i) {
        id = i;
    }

    @Column(name=diagnosis_column)
    public String getDiagnosis() {
        return diagnosis;
    }
    
    public void setDiagnosis(String d) {
        diagnosis = d;
    }

    //@Temporal(TemporalType.DATE)
    @Column(name=date_column, nullable = false)
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date d) {
        date = d;
    }

    //@Column(name=doctor_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=doctor_column, nullable = false)//(name=User.id_column)
    public User getDoctor() {
        return doctor;
    }
    
    public void setDoctor(User d) {
        doctor = d;
    }


    @ManyToOne(fetch= FetchType.LAZY)
    //@JoinColumn(name=User.id_column)
    @JoinColumn(name=patient_column, nullable = false)
    public User getPatient() {
        return patient;
    }
    
    public void setPatient(User p) {
        patient = p;
    }


    @OneToOne(fetch= FetchType.LAZY)
    //@JoinColumn(name=ScheduleTime.id_column)
    @JoinColumn(name=ScheduleTime.id_column)
    public ScheduleTime getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(ScheduleTime scheduleTime) {
        this.scheduleTime = scheduleTime;
    }
    
    /*private ArrayList<Referral> referrals;
    
    private ArrayList<Prescription> prescriptions;

    public ArrayList<Referral> getReferrals() {
        return referrals;
    }

    public void setReferrals(ArrayList<Referral> referrals) {
        this.referrals = referrals;
    }

    public ArrayList<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(ArrayList<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }*/

}
