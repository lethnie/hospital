package ru.vsu.cs.hospital.dto;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 24.05.14
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
public class ProcDTO {
    public ProcDTO() {}
    private String nurse;
    private String patient;
    private String result;
    private String type;
    private Integer count;
    private Integer id;

    public String getNurse() {
        return nurse;
    }

    public void setNurse(String nurse) {
        this.nurse = nurse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
