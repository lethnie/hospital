/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author 123
 */

@Entity
@Table(name="prescription")
public class Prescription implements Serializable {
    public static final String id_column = "prescription_id";
    public static final String patient_column = "patient_id";
    public static final String nurse_column = "nurse_id";
    public static final String count_column = "count";
    //public static final String treatment_column = "treatment_id";
    //public static final String app_column = "appointment_id";

    public Prescription() {
    }
    private Integer id;
    private Treatment treatment;
    private User nurse;
    private User patient;
    private Integer count;
    private Appointment appointment;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="prescription_sequence")
    @GenericGenerator(name="prescription_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //@Column(name=treatment_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Treatment.id_column, nullable = false)
    public Treatment getTreatment() {
        return treatment;
    }

    public void setTreatment(Treatment treatment) {
        this.treatment = treatment;
    }

    //@Column(name=nurse_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=nurse_column, nullable = false)
    public User getNurse() {
        return nurse;
    }

    public void setNurse(User nurse) {
        this.nurse = nurse;
    }

    //@Column(name=patient_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=patient_column, nullable = false)
    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    @Column(name=count_column, nullable = false)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    //@Column(name=app_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Appointment.id_column, nullable = false)
    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
}
