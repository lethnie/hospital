package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.TreatmentDAO;
import ru.vsu.cs.hospital.dao.TreatmentDAOImpl;
import ru.vsu.cs.hospital.domain.Treatment;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
@Service
public class TreatmentServiceImpl implements TreatmentService {
    @Autowired
    private TreatmentDAO treatmentDAO;

    @Transactional
    public void addTreatment(Treatment treatment) {
        treatmentDAO.addTreatment(treatment);
    }

    @Transactional
    public List<Treatment> listTreatment() {

        return treatmentDAO.listTreatment();
    }

    @Transactional
    public Treatment findTreatmentById(Integer id) {
        return treatmentDAO.findTreatmentById(id);
    }

    @Transactional
    public Treatment findTreatmentByName(String name) {
        return treatmentDAO.findTreatmentByName(name);
    }

    @Transactional
    public void removeContact(Integer id) {
        treatmentDAO.removeTreatment(id);
    }
}
