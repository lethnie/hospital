package ru.vsu.cs.hospital.service;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.Specialization;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
public interface SpecializationService {
    public void addSpecialization(Specialization specialization);
    public List<Specialization> listSpecialization();
    public Specialization findSpecializationById(Integer id);
    public void removeContact(Integer id);
}
