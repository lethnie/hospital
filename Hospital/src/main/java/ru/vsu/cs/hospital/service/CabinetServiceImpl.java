package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.CabinetDAO;
import ru.vsu.cs.hospital.dao.CabinetDAOImpl;
import ru.vsu.cs.hospital.domain.Cabinet;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.Specialization;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:12
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CabinetServiceImpl implements CabinetService {
    @Autowired
    private CabinetDAO cabinetDAO;

    @Transactional
    public void addCabinet(Cabinet cabinet) {
        cabinetDAO.addCabinet(cabinet);
    }

    @Transactional
    public List<Cabinet> listCabinet() {
        return cabinetDAO.listCabinet();
    }

    @Transactional
    public List<Cabinet> findCabinetsBySpecialization(Specialization specialization) {
        return cabinetDAO.findCabinetsBySpecialization(specialization);
    }

    @Transactional
    public Cabinet findCabinetById(Integer id) {
        return cabinetDAO.findCabinetById(id);
    }

    @Transactional
    public void removeContact(Integer id) {
        cabinetDAO.removeCabinet(id);
    }
}
