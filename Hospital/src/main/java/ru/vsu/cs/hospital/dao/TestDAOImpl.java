package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 18:08
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class TestDAOImpl implements TestDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addTest(Test test) {
        sessionFactory.getCurrentSession().save(test);
    }

    @SuppressWarnings("unchecked")
    public List<Test> listTest() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Test")
                .list();
    }

    public Test findTestById(Integer id) {
        Test result = (Test)sessionFactory.getCurrentSession().load(Test.class, id);
                /*createCriteria(Test.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();*/
        /*if (result != null) {
            Hibernate.initialize(result.);
            Hibernate.initialize(result.getSpecialization());
            Hibernate.initialize(result.getDoctor());
            sessionFactory.getCurrentSession().update(result);
        }*/
        return result;
    }

    public Test findTestByName(String name) {
        Test result = (Test)sessionFactory.getCurrentSession().createCriteria(Test.class)
                .add(Restrictions.eq("test", name))
                .uniqueResult();
        /*if (result != null) {
            Hibernate.initialize(result.);
            Hibernate.initialize(result.getSpecialization());
            Hibernate.initialize(result.getDoctor());
            sessionFactory.getCurrentSession().update(result);
        }*/
        return result;
    }

    public void removeTest(Integer id) {
        Test test = (Test) sessionFactory.getCurrentSession().load(
                Test.class, id);
        if (null != test) {
            sessionFactory.getCurrentSession().delete(test);
        }

    }
}
