package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.SpecializationDAO;
import ru.vsu.cs.hospital.dao.SpecializationDAOImpl;
import ru.vsu.cs.hospital.domain.Specialization;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SpecializationServiceImpl implements SpecializationService{
    @Autowired
    private SpecializationDAO specializationDAO;

    @Transactional
    public void addSpecialization(Specialization specialization) {
        specializationDAO.addSpecialization(specialization);
    }

    @Transactional
    public List<Specialization> listSpecialization() {

        return specializationDAO.listSpecialization();
    }

    @Transactional
    public Specialization findSpecializationById(Integer id) {
        return specializationDAO.findSpecializationById(id);
    }

    @Transactional
    public void removeContact(Integer id) {
        specializationDAO.removeSpecialization(id);
    }
}
