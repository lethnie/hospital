package ru.vsu.cs.hospital.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class RoleDAOImpl implements RoleDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addRole(Role role) {
        sessionFactory.getCurrentSession().save(role);
    }

    @SuppressWarnings("unchecked")
    public List<Role> listRole() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Role")
                .list();
    }

    public Role findById(Integer id) {
        //return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Role")
        return (Role)sessionFactory.getCurrentSession().createCriteria(Role.class)
                .add(Restrictions.eq("id_role", id)).uniqueResult();
    }

    public Role findByName(String name) {
        return (Role)sessionFactory.getCurrentSession().createCriteria(Role.class)
                .add(Restrictions.eq("role", name)).uniqueResult();
    }

    public void removeRole(Integer id) {
        Role role = (Role) sessionFactory.getCurrentSession().load(
                Role.class, id);
        if (null != role) {
            sessionFactory.getCurrentSession().delete(role);
        }

    }
}
