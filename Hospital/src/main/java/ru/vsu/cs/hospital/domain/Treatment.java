/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author 123
 */

@Entity
@Table(name="treatment")
public class Treatment implements Serializable{
    public static final String id_column = "treatment_id";
    public static final String treatment_column = "treatment_name";

    public Treatment() {        
    }

    private Integer id;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="treatment_sequence")
    @GenericGenerator(name="treatment_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer i) {
        id = i;
    }
    
    private String treatment;

    @Column(name=treatment_column, nullable = false)
    public String getTreatment() {
        return treatment;
    }
    
    public void setTreatment(String t) {
        treatment = t;
    }
}
