package ru.vsu.cs.hospital.dto;

import ru.vsu.cs.hospital.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 30.04.14
 * Time: 0:51
 * To change this template use File | Settings | File Templates.
 */
public class NewAppointmentUserDTO {
    public NewAppointmentUserDTO() {
    }

    private Integer day;
    private Integer doctor;
    private Integer start;
    private Integer patient;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getDoctor() {
        return doctor;
    }

    public void setDoctor(Integer doctor) {
        this.doctor = doctor;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPatient() {
        return patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }
}
