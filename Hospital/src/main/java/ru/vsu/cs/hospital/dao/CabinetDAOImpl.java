package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 22:30
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CabinetDAOImpl implements CabinetDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addCabinet(Cabinet cabinet) {
        sessionFactory.getCurrentSession().save(cabinet);
    }

    @SuppressWarnings("unchecked")
    public List<Cabinet> listCabinet() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Cabinet")
                .list();
    }

    public List<Cabinet> findCabinetsBySpecialization(Specialization specialization) {
        List<Cabinet> result = sessionFactory.getCurrentSession().createCriteria(Cabinet.class)
                .add(Restrictions.eq("specialization", specialization))
                .list();
        for (Cabinet cabinet : result) {
            Hibernate.initialize(cabinet.getSpecialization());
            sessionFactory.getCurrentSession().update(cabinet);
        }

        return result;
    }

    public Cabinet findCabinetById(Integer id) {
        Cabinet result = (Cabinet)sessionFactory.getCurrentSession().createCriteria(Cabinet.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        if (result != null) {
            Hibernate.initialize(result.getSpecialization());
            sessionFactory.getCurrentSession().update(result);
        }
        return result;
    }

    public void removeCabinet(Integer id) {
        Cabinet cabinet = (Cabinet) sessionFactory.getCurrentSession().load(
                Cabinet.class, id);
        if (null != cabinet) {
            sessionFactory.getCurrentSession().delete(cabinet);
        }

    }
}
