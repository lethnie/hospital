package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.AppointmentTime;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public interface AppointmentTimeDAO {
    public void addAppointmentTime(AppointmentTime appointmentTime);
    public List<AppointmentTime> listAppointmentTime();
    public void removeAppointmentTime(Integer id);
}
