package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 12:49
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="specialization")
public class Specialization implements Serializable{
    public static final String id_column = "spec_id";
    public static final String spec_column = "spec_name";
    //public static final String cab_column = "cabinets";

    public Specialization() {
    }

    private Integer id;
    private String spec_name;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="specialization_sequence")
    @GenericGenerator(name="specialization_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name=spec_column, nullable = false)
    public String getSpec_name() {
        return spec_name;
    }

    public void setSpec_name(String spec_name) {
        this.spec_name = spec_name;
    }

    /*@OneToMany(fetch = FetchType.LAZY,
            mappedBy = "companyIdRef")
    private ArrayList<Cabinet> cabinets;*/
}
