package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.AppointmentDAO;
import ru.vsu.cs.hospital.dao.AppointmentDAOImpl;
import ru.vsu.cs.hospital.domain.Appointment;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.User;

import java.sql.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:00
 * To change this template use File | Settings | File Templates.
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {
    @Autowired
    private AppointmentDAO appointmentDAO;

    @Transactional
    public void addAppointment(Appointment appointment) {
        appointmentDAO.addAppointment(appointment);
    }

    @Transactional
    public List<Appointment> listAppointment() {

        return appointmentDAO.listAppointment();
    }

    @Transactional
    public void removeContact(Integer id) {
        appointmentDAO.removeAppointment(id);
    }

    @Transactional
    public List<Appointment> getAppointmentsByPatient(User user) {
        return appointmentDAO.getAppointmentsByPatient(user);
    }

    @Transactional
    public List<Appointment> getAppointmentsByDoctor(User user) {
        return appointmentDAO.getAppointmentsByDoctor(user);
    }

    @Transactional
    public List<Appointment> getAppointmentsByDoctorAndDay(User user, Date date) {
        return appointmentDAO.getAppointmentsByDoctorAndDay(user, date);
    }

    @Transactional
    public Appointment getAppointmentById(Integer id) {
        return appointmentDAO.getAppointmentById(id);
    }

    @Transactional
    public void updateAppointment(Appointment appointment) {
         appointmentDAO.updateAppointment(appointment);
    }
}
