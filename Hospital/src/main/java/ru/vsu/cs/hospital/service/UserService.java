package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Role;
import ru.vsu.cs.hospital.domain.Specialization;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 19.04.14
 * Time: 0:31
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {

    public void addUser(User user);

    public List<User> listUser();

    public User findUser(String name, String password, Role role);

    public User findUserById(Integer id);

    public User findUserByName(String name);

    public List<User> findUsersByRole(Role role);

    public List<User> findNursesByDoctor(User doctor);

    public List<User> findUsersBySpecialization(Specialization specialization);

    public void removeContact(Integer id);
}
