package ru.vsu.cs.hospital.service;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.User;

import java.sql.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 19.04.14
 * Time: 0:30
 * To change this template use File | Settings | File Templates.
 */
public interface AppointmentService {
    public void addAppointment(Appointment appointment);
    public List<Appointment> listAppointment();
    public void removeContact(Integer id);
    public List<Appointment> getAppointmentsByPatient(User user);
    public List<Appointment> getAppointmentsByDoctor(User user);
    public List<Appointment> getAppointmentsByDoctorAndDay(User user, Date date);
    public Appointment getAppointmentById(Integer id);
    public void updateAppointment(Appointment appointment);
}
