package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Schedule;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public interface ScheduleDAO {
    public void addSchedule(Schedule schedule);
    public List<Schedule> listSchedule();
    public Schedule findScheduleByDayAndDoctor(Integer day, User doctor);
    public void removeSchedule(Integer id);
}
