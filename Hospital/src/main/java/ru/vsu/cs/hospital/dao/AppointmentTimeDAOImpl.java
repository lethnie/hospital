package ru.vsu.cs.hospital.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AppointmentTimeDAOImpl implements AppointmentTimeDAO{
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addAppointmentTime(AppointmentTime appointmentTime) {
        sessionFactory.getCurrentSession().save(appointmentTime);
    }

    @SuppressWarnings("unchecked")
    public List<AppointmentTime> listAppointmentTime() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.AppointmentTime")
                .list();
    }

    public void removeAppointmentTime(Integer id) {
        AppointmentTime appointment = (AppointmentTime) sessionFactory.getCurrentSession().load(
                AppointmentTime.class, id);
        if (null != appointment) {
            sessionFactory.getCurrentSession().delete(appointment);
        }

    }
}
