package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Schedule;
import ru.vsu.cs.hospital.domain.ScheduleTime;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
public interface ScheduleTimeService {
    public void addScheduleTime(ScheduleTime referral);
    public void updateScheduleTime(ScheduleTime referral);
    public List<ScheduleTime> listScheduleTime();
    public ScheduleTime findScheduleTimeById(Integer id);
    public void removeContact(Integer id);
    public List<ScheduleTime> findFreeScheduleTimeBySchedule(Schedule schedule);
    public List<ScheduleTime> findScheduleTimeBySchedule(Schedule schedule);
    //public void setScheduleTimeFree(ScheduleTime st, Boolean isFree);
}
