package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Specialization;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public interface SpecializationDAO {
    public void addSpecialization(Specialization specialization);
    public List<Specialization> listSpecialization();
    public Specialization findSpecializationById(Integer id);
    public void removeSpecialization(Integer id);
}