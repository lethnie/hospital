package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.User;
import java.sql.Date;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:01
 * To change this template use File | Settings | File Templates.
 */
public interface AppointmentDAO {
    public void addAppointment(Appointment appointment);
    public List<Appointment> listAppointment();
    public List<Appointment> getAppointmentsByPatient(User user);
    public List<Appointment> getAppointmentsByDoctor(User user);
    public List<Appointment> getAppointmentsByDoctorAndDay(User user, Date date);
    public Appointment getAppointmentById(Integer id);
    public void updateAppointment(Appointment appointment);
    public void removeAppointment(Integer id);
}
