package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ReferralDAOImpl implements ReferralDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addReferral(Referral referral) {
        sessionFactory.getCurrentSession().save(referral);
    }

    @SuppressWarnings("unchecked")
    public List<Referral> listReferral() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Referral")
                .list();
    }

    public List<Referral> getReferralsByPatient(User user) {
        List<Referral> referrals;
        referrals = sessionFactory.getCurrentSession().createCriteria(Referral.class)
                .add(Restrictions.eq("patient", user)).list();
        for (Referral referral : referrals) {
            Hibernate.initialize(referral.getNurse());
            Hibernate.initialize(referral.getPatient());
            Hibernate.initialize(referral.getTest());
            Hibernate.initialize(referral.getAppointment());
        }
        return referrals;
    }

    public List<Referral> getReferralsByNurse(User user) {
        List<Referral> referrals;
        referrals = sessionFactory.getCurrentSession().createCriteria(Referral.class)
                .add(Restrictions.eq("nurse", user)).list();
        for (Referral referral : referrals) {
            Hibernate.initialize(referral.getNurse());
            Hibernate.initialize(referral.getPatient());
            Hibernate.initialize(referral.getTest());
            Hibernate.initialize(referral.getAppointment());
        }
        return referrals;
    }

    public List<Referral> getReferralsByAppointment(Appointment appointment) {
        List<Referral> referrals;
        referrals = sessionFactory.getCurrentSession().createCriteria(Referral.class)
                .add(Restrictions.eq("appointment", appointment)).list();
        for (Referral referral : referrals) {
            Hibernate.initialize(referral.getNurse());
            Hibernate.initialize(referral.getPatient());
            Hibernate.initialize(referral.getTest());
            Hibernate.initialize(referral.getAppointment());
        }
        return referrals;
    }

    public Referral getReferralById(Integer id) {
        Referral result = (Referral)sessionFactory.getCurrentSession().createCriteria(Referral.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        Hibernate.initialize(result.getTest());
        Hibernate.initialize(result.getPatient());
        Hibernate.initialize(result.getNurse());
        Hibernate.initialize(result.getAppointment());
        sessionFactory.getCurrentSession().update(result);
        return result;
    }

    public void updateReferral(Referral referral) {
        sessionFactory.getCurrentSession().update(referral);
    }

    public void removeReferral(Integer id) {
        Referral referral = (Referral) sessionFactory.getCurrentSession().load(
                Referral.class, id);
        if (null != referral) {
            sessionFactory.getCurrentSession().delete(referral);
        }

    }
}
