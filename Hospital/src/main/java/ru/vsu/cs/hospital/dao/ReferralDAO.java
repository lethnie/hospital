package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.Referral;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public interface ReferralDAO {
    public void addReferral(Referral referral);
    public List<Referral> listReferral();
    public List<Referral> getReferralsByPatient(User user);
    public List<Referral> getReferralsByNurse(User user);
    public List<Referral> getReferralsByAppointment(Appointment appointment);
    public Referral getReferralById(Integer id);
    public void updateReferral(Referral referral);
    public void removeReferral(Integer id);
}
