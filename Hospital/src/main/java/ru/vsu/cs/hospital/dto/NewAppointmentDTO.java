package ru.vsu.cs.hospital.dto;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 23.05.14
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 */
public class NewAppointmentDTO {
    public NewAppointmentDTO() {
    }

    private int id;
    private String time;
    private String patient;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }
}
