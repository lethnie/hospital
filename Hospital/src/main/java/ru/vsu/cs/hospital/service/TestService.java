package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
public interface TestService {
    public void addTest(Test test);
    public List<Test> listTest();
    public Test findTestById(Integer id);
    public Test findTestByName(String name);
    public void removeContact(Integer id);
}
