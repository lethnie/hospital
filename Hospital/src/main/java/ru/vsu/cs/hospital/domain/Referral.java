/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author 123
 */
@Entity
@Table(name="referral")
public class Referral implements Serializable {
    public static final String id_column = "referral_id";
    public static final String patient_column = "patient_id";
    public static final String nurse_column = "nurse_id";
    public static final String result_column = "result";
    //public static final String test_column = "test_id";
    //public static final String app_column = "appointment_id";

    public Referral() {
    }

    private Integer id;
    private Test test;
    private User nurse;
    private User patient;
    private String result;
    private Appointment appointment;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="referral_sequence")
    @GenericGenerator(name="referral_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //@Column(name=test_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Test.id_column, nullable = false)
    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    //@Column(name=nurse_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=nurse_column, nullable = false)
    public User getNurse() {
        return nurse;
    }

    public void setNurse(User nurse) {
        this.nurse = nurse;
    }

    //@Column(name=patient_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=patient_column, nullable = false)
    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    @Column(name=result_column, nullable = false)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    //@Column(name=app_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Appointment.id_column, nullable = false)
    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
}
