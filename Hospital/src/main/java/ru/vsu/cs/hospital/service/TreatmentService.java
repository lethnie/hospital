package ru.vsu.cs.hospital.service;

import ru.vsu.cs.hospital.domain.Treatment;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public interface TreatmentService {

    public void addTreatment(Treatment treatment);
    public List<Treatment> listTreatment();
    public Treatment findTreatmentById(Integer id);
    public Treatment findTreatmentByName(String name);
    public void removeContact(Integer id);
}
