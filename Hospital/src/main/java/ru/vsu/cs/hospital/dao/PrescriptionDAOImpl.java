package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 22:35
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PrescriptionDAOImpl implements PrescriptionDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addPrescription(Prescription prescription) {
        sessionFactory.getCurrentSession().save(prescription);
    }

    @SuppressWarnings("unchecked")
    public List<Prescription> listPrescription() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Prescription")
                .list();
    }

    public List<Prescription> getPrescriptionsByUser(User user) {
            List<Prescription> prescriptions;
        prescriptions = sessionFactory.getCurrentSession().createCriteria(Prescription.class)
                    .add(Restrictions.eq("patient", user)).list();
            for (Prescription prescription : prescriptions) {
                Hibernate.initialize(prescription.getNurse());
                Hibernate.initialize(prescription.getPatient());
                Hibernate.initialize(prescription.getTreatment());
                Hibernate.initialize(prescription.getAppointment());
            }
            return prescriptions;
    }

    public List<Prescription> getPrescriptionsByNurse(User user) {
        List<Prescription> prescriptions;
        prescriptions = sessionFactory.getCurrentSession().createCriteria(Prescription.class)
                .add(Restrictions.eq("nurse", user))
                .add(Restrictions.gt("count", 0))
                .list();
        for (Prescription prescription : prescriptions) {
            Hibernate.initialize(prescription.getNurse());
            Hibernate.initialize(prescription.getPatient());
            Hibernate.initialize(prescription.getTreatment());
            Hibernate.initialize(prescription.getAppointment());
        }
        return prescriptions;
    }

    public List<Prescription> getPrescriptionsByAppointment(Appointment appointment) {
        List<Prescription> prescriptions;
        prescriptions = sessionFactory.getCurrentSession().createCriteria(Prescription.class)
                .add(Restrictions.eq("appointment", appointment)).list();
        for (Prescription prescription : prescriptions) {
            Hibernate.initialize(prescription.getNurse());
            Hibernate.initialize(prescription.getPatient());
            Hibernate.initialize(prescription.getTreatment());
            Hibernate.initialize(prescription.getAppointment());
        }
        return prescriptions;
    }

    public Prescription getPrescriptionById(Integer id) {
        Prescription result = (Prescription)sessionFactory.getCurrentSession().createCriteria(Prescription.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        Hibernate.initialize(result.getTreatment());
        Hibernate.initialize(result.getPatient());
        Hibernate.initialize(result.getNurse());
        Hibernate.initialize(result.getAppointment());
        sessionFactory.getCurrentSession().update(result);
        return result;
    }

    public void updatePrescription(Prescription prescription) {
        sessionFactory.getCurrentSession().update(prescription);
    }

    public void removePrescription(Integer id) {
        Prescription prescription = (Prescription) sessionFactory.getCurrentSession().load(
                Prescription.class, id);
        if (null != prescription) {
            sessionFactory.getCurrentSession().delete(prescription);
        }

    }
}
