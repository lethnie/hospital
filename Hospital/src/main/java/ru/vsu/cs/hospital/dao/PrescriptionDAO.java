package ru.vsu.cs.hospital.dao;

import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.Prescription;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 20.04.14
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public interface PrescriptionDAO {
    public void addPrescription(Prescription prescription);
    public List<Prescription> listPrescription();
    public List<Prescription> getPrescriptionsByUser(User user);
    public List<Prescription> getPrescriptionsByNurse(User user);
    public List<Prescription> getPrescriptionsByAppointment(Appointment appointment);
    public Prescription getPrescriptionById(Integer id);
    public void updatePrescription(Prescription prescription);
    public void removePrescription(Integer id);
}
