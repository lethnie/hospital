package ru.vsu.cs.hospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.cs.hospital.dao.ReferralDAO;
import ru.vsu.cs.hospital.dao.ReferralDAOImpl;
import ru.vsu.cs.hospital.domain.Appointment;
import ru.vsu.cs.hospital.domain.Referral;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 18.04.14
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ReferralServiceImpl implements ReferralService {
    @Autowired
    private ReferralDAO referralDAO;

    @Transactional
    public void addReferral(Referral referral) {
        referralDAO.addReferral(referral);
    }

    @Transactional
    public List<Referral> listReferral() {

        return referralDAO.listReferral();
    }

    @Transactional
    public List<Referral> getReferralsByUser(User user) {
        return referralDAO.getReferralsByPatient(user);
    }

    @Transactional
    public List<Referral> getReferralsByNurse(User user) {
        return referralDAO.getReferralsByNurse(user);
    }

    @Transactional
    public List<Referral> getReferralsByAppointment(Appointment appointment) {
        return referralDAO.getReferralsByAppointment(appointment);
    }

    @Transactional
    public Referral getReferralById(Integer id) {
        return referralDAO.getReferralById(id);
    }

    @Transactional
    public void updateReferral(Referral referral) {
        referralDAO.updateReferral(referral);
    }

    @Transactional
    public void removeContact(Integer id) {
        referralDAO.removeReferral(id);
    }
}
