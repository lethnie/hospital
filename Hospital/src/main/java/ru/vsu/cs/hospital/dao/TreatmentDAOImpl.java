package ru.vsu.cs.hospital.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class TreatmentDAOImpl implements TreatmentDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addTreatment(Treatment treatment) {
        sessionFactory.getCurrentSession().save(treatment);
    }

    @SuppressWarnings("unchecked")
    public List<Treatment> listTreatment() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Treatment")
                .list();
    }

    public Treatment findTreatmentById(Integer id) {
        Treatment result = (Treatment)sessionFactory.getCurrentSession().load(Treatment.class, id);
        return result;
    }

    public Treatment findTreatmentByName(String name) {
        Treatment result = (Treatment)sessionFactory.getCurrentSession().createCriteria(Treatment.class)
                .add(Restrictions.eq("treatment", name))
                .uniqueResult();
        return result;
    }

    public void removeTreatment(Integer id) {
        Treatment treatment = (Treatment) sessionFactory.getCurrentSession().load(
                Treatment.class, id);
        if (null != treatment) {
            sessionFactory.getCurrentSession().delete(treatment);
        }

    }
}
