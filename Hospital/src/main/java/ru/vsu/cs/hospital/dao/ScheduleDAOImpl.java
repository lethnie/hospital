package ru.vsu.cs.hospital.dao;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.vsu.cs.hospital.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 04.04.14
 * Time: 17:50
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ScheduleDAOImpl implements ScheduleDAO {
    @Autowired
    private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();

    public void addSchedule(Schedule schedule) {
        sessionFactory.getCurrentSession().save(schedule);
    }

    @SuppressWarnings("unchecked")
    public List<Schedule> listSchedule() {

        return sessionFactory.getCurrentSession().createQuery("from ru.vsu.cs.hospital.domain.Schedule")
                .list();
    }

    public Schedule findScheduleByDayAndDoctor(Integer day, User doctor) {
        //System.out.println("DAY: " + day + ", DOCTOR: " + doctor.getId());
        Schedule result = (Schedule)sessionFactory.getCurrentSession().createCriteria(Schedule.class)
                .add(Restrictions.eq("day", day))
                .add(Restrictions.eq("doctor", doctor))
                .uniqueResult();
        if (result == null)
            return null;
        //System.out.println("SCHEDULE AFTER DAY AND DOC");
        Hibernate.initialize(result.getCabinet());
        Hibernate.initialize(result.getDoctor());
        sessionFactory.getCurrentSession().update(result);

        return result;
    }

    public void removeSchedule(Integer id) {
        Schedule schedule = (Schedule) sessionFactory.getCurrentSession().load(
                Schedule.class, id);
        if (null != schedule) {
            sessionFactory.getCurrentSession().delete(schedule);
        }

    }
}
