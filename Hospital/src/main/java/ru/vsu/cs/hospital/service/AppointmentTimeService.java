package ru.vsu.cs.hospital.service;

import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.hospital.domain.AppointmentTime;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 19.04.14
 * Time: 0:31
 * To change this template use File | Settings | File Templates.
 */
public interface AppointmentTimeService {
    public void addAppointmentTime(AppointmentTime appointmentTime);
    public List<AppointmentTime> listAppointmentTime();
    public void removeContact(Integer id);
}
