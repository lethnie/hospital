package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="schedule")
public class Schedule implements Serializable {
    public static final String id_column = "schedule_id";
    public static final String day_column = "day";
    //public static final String cab_column = "cabinet_id";
    //public static final String doctor_column = "doctor_id";

    public Schedule() {
    }

    private Integer id;
    private Cabinet cabinet;
    private User doctor;
    private Integer day;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="schedule_sequence")
    @GenericGenerator(name="schedule_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //@Column(name=cab_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=Cabinet.id_column, nullable = false)
    public Cabinet getCabinet() {
        return cabinet;
    }

    public void setCabinet(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    //@Column(name=doctor_column, nullable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name=User.id_column, nullable = false)
    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    @Column(name=day_column, nullable = false)
    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
