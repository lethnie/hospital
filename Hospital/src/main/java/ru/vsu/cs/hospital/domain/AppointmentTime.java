package ru.vsu.cs.hospital.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: 123
 * Date: 03.04.14
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="app_time")
public class AppointmentTime implements Serializable {
    public static final String id_column = "app_time_id";
    public static final String start_column = "start";
    public static final String finish_column = "finish";

    public AppointmentTime() {
    }

    private Integer id;
    private String start;
    private String finish;

    @Id
    @Column(name=id_column)
    @GeneratedValue(generator="appointment_time_sequence")
    @GenericGenerator(name="appointment_time_sequence", strategy = "increment")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name=start_column, nullable = false)
    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    @Column(name=finish_column, nullable = false)
    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }
}
